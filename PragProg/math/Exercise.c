#include "stdio.h"
#include "tgmath.h"



int main(){
	
double a;
a=tgamma(5);
printf("Gamma function to 5 is %g\n", a );

double b;
b=j1(0.5);
printf("First order of Besel function to 0.5 is %g\n", b );

double complex c;
c=csqrt(-2.0);
printf("Negative sqaure root of -2 is %g%+gi\n", creal(c),cimag(c) );

double complex d;
d=cexp(I);
printf("Exponential function of I is %g%+gi\n", creal(d),cimag(d));

double complex e;
e=cexp(I*M_PI);
printf("Exponential function of I times Pi is %g%+gi\n", creal(e),cimag(e) );

double complex f;
f=pow(I,M_E);
printf("I to the power of exponential function is %g%+gi\n", creal(f),cimag(f) );

float q=0.1111111111111111111111111111;
double w=0.1111111111111111111111111111;
long double r=0.1111111111111111111111111111;


	printf ("0.1111111111111111111111111111 in float       : %.25g\n",  q);
  	printf ("0.1111111111111111111111111111 in double      : %.25lg\n", w);
  	printf ("0.1111111111111111111111111111 in long double : %.25Lg\n\n\n\n\n", r);



// opgave iii) The machine epsilon is the difference between 1.0 and the next representable floating point number. 
//Using the while loop calculate the machine epsilon for types float, double, and long double,
// and compare with the values FLT_EPSILON, DBL_EPSILON, and LDBL_EPSILON defined in float.h.

printf("FLT_EPSILON is %g\n",FLT_EPSILON);
printf("DBL_EPSILON is  %g\n",DBL_EPSILON);
printf("LDBL_EPSILON %Lg\n\n", LDBL_EPSILON);


float fe_while= 1;
while(1+fe_while != 1){fe_while/=2;} fe_while*=2;
printf("float epsilon using while is = %g\n",fe_while);

double de_while = 1;
while(1+de_while!=1) {de_while/=2;} de_while*=2;
printf("double epsilon using while = %g\n",de_while);

long double le_while = 1.0;
while(1+le_while!=1) {le_while/=2;} le_while*=2;
printf("long epsilon using while = %Lg\n\n",le_while);



float fe_for=1;
for (fe_for; 1+fe_for!=1; fe_for/=2){} fe_for*=2;
	printf("float epsilon using for is = %g\n",fe_for);

double de_for=1;
for (de_for; 1+de_for!=1; de_for/=2){} de_for*=2;
	printf("double epsilon using for is = %g\n",de_for);

long double le_for=1;
for (le_for; 1+le_for!=1; le_for/=2){} le_for*=2;
	printf("long epsilon using for is = %Lg\n\n",le_for);




float fe_do=1;
do
{fe_do/=2;}
while(1+fe_do!=1); 

printf("float epsilon for do while= %g\n",fe_do*=2); 



double de_do=1;
do
{de_do/=2;}
while(1+de_do!=1); 

printf("double epsilon for do while= %g\n",de_do*=2); 


long double le_do=1;
do
{le_do/=2;}
while(1+le_do!=1); 

printf("long epsilon for do while= %Lg\n",le_do*=2); 



	return 0;
}