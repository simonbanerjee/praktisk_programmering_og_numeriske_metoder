#include <stdio.h>
#include <math.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>

//First we create a function where we define our differential equation
int logistic_diff_equation(double t, const double y[], double dydt[], void * params)
{
  dydt[0]=y[1];
  dydt[0]=y[0]*(1-y[0]);
  return GSL_SUCCESS;
}

// Now se set our initial conditions and other limitations
double logistic_function(double x){
  gsl_odeiv2_system sys;
  sys.function = logistic_diff_equation;
  sys.jacobian = NULL;
  sys.dimension = 1;
  sys.params = NULL;

double acc = 1e-6; // accurracy
double eps = 1e-6; // epsilon
double hstart = copysign(0.1,x); // Allocating driver

gsl_odeiv2_driver* driver=
	gsl_odeiv2_driver_alloc_y_new
		(&sys, gsl_odeiv2_step_rkf45,hstart,acc,eps);

double t=0; // start from
double y[1]={0.5}; // initial conditions
gsl_odeiv2_driver_apply(driver, &t,x,y); // here we apply the driver made above

gsl_odeiv2_driver_free(driver);

return y[0];
}

double log_fun_ex(double x){
  double logistic_function=1.0/(1.0+exp(-x)); //This is an analytic expression of the fucntion
return logistic_function;
}

int main() {
double a=0;
double b=3;
double dx=0.1;
printf("x \t y \t logistic \n");
for (double x=a; x<=b; x+=dx)
	{
printf("%g %g %g\n", x, logistic_function(x), log_fun_ex(x)); // This prints both the numerical and analytical values
	}

  return 0;
}
