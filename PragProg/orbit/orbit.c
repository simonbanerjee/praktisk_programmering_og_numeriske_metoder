#include <stdio.h>
#include <math.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>
// We define our diff equation. We build it as two first order diff equation instead as 1 second order diff equation
int equatorial_motion_equation(double phi, const double y[], double yprime[], void * params)
{
  double epsilon = *(double *) params;
  yprime[0]=y[1];
  yprime[1]=1.0+epsilon*y[0]*y[0]-y[0];
  return GSL_SUCCESS;
}

// Now we define the functions that should solve our diff equation using different initial conditions and relativistic corrections.
double equatorial_motion(double x, double epsilon, double u_i, double up_i){
// We define our system
gsl_odeiv2_system orbit;
orbit.function = equatorial_motion_equation;
orbit.jacobian = NULL;
orbit.dimension = 2;
orbit.params = (void *) &epsilon;

double acc = 1e-6; // accurracy
double eps = 1e-6; // epsilon
double hstart = copysign(0.1,x); // Allocating driver

gsl_odeiv2_driver* driver= gsl_odeiv2_driver_alloc_y_new
		(&orbit, gsl_odeiv2_step_rkf45,hstart,acc,eps); // we allocate driver

double t=0, y[2]={u_i,up_i};
gsl_odeiv2_driver_apply(driver,&t,x,y); // here we apply the driver a solve our system
gsl_odeiv2_driver_free(driver);
return y[0];

}

int main(){
double epsilon_i=0.0;
double epsilon_ii=0.0;
double epsilon_iii=0.01;
double a=0;
double b=4*2*M_PI;
double dx=0.1;
printf("x \t c \t epsilon_ii \t epsilon_iii\n");
for (double x=a; x<=b; x+=dx)
	{
printf("%g %g %g %g\n", x, equatorial_motion(x,epsilon_i,1,0), equatorial_motion(x,epsilon_ii,1,-0.5), equatorial_motion(x,epsilon_iii,1,-0.5)); // This prints the values
	}

  return 0;
}
