#include <stdio.h>
#include <math.h>
#include "gsl/gsl_multiroots.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_odeiv2.h"
#include "gsl/gsl_errno.h"

// We define our function

int rosenbrock_fun(const gsl_vector * x, void *params,gsl_vector * f){
double x1 = gsl_vector_get(x,0);
double x2 = gsl_vector_get(x,1);

// now we derive our function f(x,y) with respect to first x anf then y

double derivative_x=2.0*( (x1-1.0)-200*(x2-x1*x1)*x1);
double derivative_y=200*(x2-x1*x1);

// Now we create a gsl vector with two entries, with first the derivative_x and then derivative_y.
gsl_vector_set(f,0,derivative_x);
gsl_vector_set(f,1,derivative_y);
return GSL_SUCCESS;
}


//Defining the funciton that calculates the function
int main() {
const int dim=2; //define the dimension of the system


gsl_multiroot_function F; //defining the system
F.f=rosenbrock_fun; //choosing which funciton it should take
F.n=dim; //choosing the dimension
F.params= NULL;

// Now choosing our solver, we choose gsl_multiroot_fsolver
// We also allocate memory for the rootsolver using gsl multiroot fsolver hybrid and dimensions dim
gsl_multiroot_fsolver * s =gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrid, dim);
gsl_vector * x = gsl_vector_alloc(dim);
gsl_vector_set(x,0,2);
gsl_vector_set(x,1,2);
gsl_multiroot_fsolver_set(s,&F,x);

//define how many iterations
int iter=0;
int status= GSL_CONTINUE;
double acc=1e-6; // accuracy


while (status==GSL_CONTINUE && iter<1000) { iter++;
  int flag=gsl_multiroot_fsolver_iterate(s); // Performs a single iteration on the solver s
if (flag!=0) break;
status = gsl_multiroot_test_residual((*s).f,acc); // Testing to see whether the wanted accuracy is obtained. If yes GSL_SUCESS is returnes and if not GSL_CONTINUE
}

printf("Iteration x y gradient_x gradient_y\n");
double x_value=gsl_vector_get(s->x,0);
double y_value=gsl_vector_get(s->x,1);

double grad_x=gsl_vector_get(s->f,0);
double grad_y=gsl_vector_get(s->f,1);

printf("%i %g %g %g %g\n",iter, x_value, y_value,grad_x, grad_y); // Printing to output file

// freeing allocated space
gsl_multiroot_fsolver_free(s);
gsl_vector_free(x);

return 0;
}
