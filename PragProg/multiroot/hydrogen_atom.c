#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include "gsl/gsl_vector.h"
#include "gsl/gsl_odeiv2.h"
#include <math.h>

int diff_equation(double r, const double y[], double yp[], void* params){
//Defining our diff eq. Instead of second order diff eq. we define it as to first order diff eq.
double e=*(double*)params;
yp[0]=y[1];
yp[1]=2*(-1/r-e)*y[0];
return GSL_SUCCESS;
}

double Fe(double e,double r){
assert(e<0); // Asumptions on energy
double r_min=1e-3;
if (r<r_min) return r-r*r;// Behaviour of wave function at small distances

int dim=2;// Dimension of system
gsl_odeiv2_system F; // Defining system
F.function=diff_equation;
F.dimension=dim;
F.params=(void*)&e;
F.jacobian=NULL;

double epsabs=1e-6, epsrel=1e-6, hstart=0.001; // Setting absolute and relative errors aswell as initial step size
  gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new (&F, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);// Allocate and initialise the evolve, control and stepper objects for the system F using the method gsl_odeiv2_step_rk8pd, initial step size hstart and the absolute and relative precision

double t=r_min, y[2]={t-t*t,1-2*t};
gsl_odeiv2_driver_apply (d,&t,r,y); // Evolving the system d from t to r.

gsl_odeiv2_driver_free (d);
return y[0];
}

int master(const gsl_vector *x, void *params, gsl_vector *f){
double e=gsl_vector_get(x,0);
assert(e<0);
double r_max=*(double*) params;
gsl_vector_set(f,0,Fe(e,r_max));
return GSL_SUCCESS;
}

int main(){
  int dim=1;
  double r_max=8;
  //Allocating memory for s
  gsl_multiroot_fsolver *s=gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids,dim);
  gsl_multiroot_function F;// we define the system

  F.f=master;
  F.n=dim;
  F.params=(void*)&r_max;
  gsl_vector *x=gsl_vector_alloc(dim);
  gsl_vector_set(x,0,-1);
  gsl_multiroot_fsolver_set(s,&F,x); //This function set or reset an existing function s to use function F and the initial guess x

  int inter=0;
  int status;
  double absolute=1e-3;
  fprintf(stderr, "iter \t e \t r_max \t H(r_max)\n");

//Now we create a do-while loop to solve the system

do {inter++;
  status=gsl_multiroot_fsolver_iterate(s); //We perform single iteration of the solver s
  status=gsl_multiroot_test_residual((*s).f,absolute); // Testing whether to see the precision are within the wanted value. If yes then return GSL_SUCCESS and if not GSL_CONTINUE
  if(status==GSL_SUCCESS) fprintf(stderr, "converged\n");
  fprintf(stderr, "%i \t %.3g \t %.3g \t %.3g \n", inter, gsl_vector_get((*s).x,0), r_max, gsl_vector_get((*s).f,0)); // this print the result for each iteration and save the data into a txt file
} while(status==GSL_CONTINUE && inter<100);

  double e=gsl_vector_get((*s).x,0);
  fprintf(stderr, "r_max, e\n");
  fprintf(stderr, "%.3g %.3g\n",r_max, e );
  fprintf(stderr, "\n\n");

  for(double r=0; r<=r_max; r+=r_max/64) printf(" %g %g %g\n", r, Fe(e,r), r*exp(-r)); // Printing values which will be used in the plot. Both numeric calculated and the analytic expression


  gsl_multiroot_fsolver_free(s);
  gsl_vector_free(x);

}
