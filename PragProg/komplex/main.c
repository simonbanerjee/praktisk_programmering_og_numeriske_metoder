#include "komplex.h"
#include <stdio.h>
#include <tgmath.h>
#include <complex.h>
#include <time.h>
#include <stdlib.h>

/*
#ifndef _clang_
#define complex _Complex double
#endif
*/


#ifdef __clang__
#define complex _Complex double
#endif
#include"komplex.h"
#include <stdlib.h>
#define RND (double)rand()/RAND_MAX

//#define KOMPLEX(z) komplex_new(creal(z),cimag(z));

int main()
{
	//srand(time(NULL)); // should only be called once
	//int random_number=rand(); // Generates a random number between 0 and RAND_MAX

	komplex a={1,2};
	komplex b={3,4};
	komplex z;
	komplex a_new;


	/*complex A = a.re + a.im*I ;
	complex B = b.re + b.im*I ;*/


	double x=3.8 ; double y=8.7;

	printf("Testing komplex_set...\n");
	komplex_print("a=",a);
	printf("Now we set a--->z\n");
	komplex_set(&z,a.re,a.im);
	komplex_print("z=",z);
	if (komplex_equal(z,a))
		printf("Test passed\n\n");
	else
		printf("Test failed \n\n");




	printf("Testing komplex_new...\n");
	a_new=komplex_new(x,y);
	komplex_print("a_new=", a_new);


	printf("\nTesting komplex_add...\n");
	komplex_print("a=",a);
	komplex_print("b=",b);
	z=komplex_add(a,b);
	komplex w= {4,6};
	komplex_print("a+b should give = ", w);
	komplex_print("a+b actually give = ", z);
	if (komplex_equal(w,z))
		printf("Test passed\n\n");
	else
		printf("Test failed \n\n");


	printf("Testing komplex_sub...\n");
	komplex_print("a=",a);
	komplex_print("b=",b);
	z=komplex_sub(a,b);
	komplex W= {-2,-2};
	komplex_print("a+b should give = ", W);
	komplex_print("a+b actually give = ", z);
	if (komplex_equal(w,z))
		printf("Test passed\n\n");
	else
		printf("Test failed \n\n");

















	return 0;
}
