#include <stdio.h>
#include "komplex.h"
#include <math.h>
#include <float.h>
#define TAU 1e-6
#define EPS 1e-6


/* #ifndef checks whether the given token has been #defined earlier in the file or in an included file; if not, it includes the code between it and the closing #else or, if no #else is present, #endif statement.
/#ifndef is often used to make header files idempotent by defining a token once the file has been included and checking that the token was not set at the top of that file. */

// We define tau and epsilon
/* #ifndef TAU
#define TAU 1e-6
#endif

#ifndef EPS
#define EPS 1e-6
#endif */


const komplex komplex_I = {0,1} ;

// We now define different functions using our komplex.h file which is included


void komplex_print(char* s, komplex a)
{
	printf("%s(%g,%g)\n",s, a.re, a.im );

}


void komplex_set (komplex* z, double x, double y){

	z->re=x; // saying that the real part of the stucture z is called x
	z->im=y; // saying that the imaginary part of the stucture z is called y
}

komplex komplex_new ( double x , double y){
		komplex z= {x,y};
		return z;

}


komplex komplex_add (komplex a, komplex b){

	komplex result_add={a.re+b.re, a.im+b.im};
	return result_add;
}


komplex komplex_sub (komplex a, komplex b){

	komplex result_sub={a.re-b.re, a.im-b.im};
	return result_sub;
}


int double_equal(double a, double b){
	if( fabs(a-b) < TAU ) return 1;
	if( fabs(a-b) < EPS*(fabs(a)+fabs(b)) ) return 1;
	return 0;
}


int komplex_equal(komplex a, komplex b){

return double_equal(a.re, b.re) && double_equal(a.im, b.im);

}
