#include <stdio.h>
#include <math.h>
#include "gsl/gsl_odeiv2.h"
#include "gsl/gsl_errno.h"

//Define the differential equation
int error_diff_function(double t, const double y[], double dydt[],void* params){

dydt[0]=2/sqrt(M_PI)*exp(-t*t);
  return GSL_SUCCESS;
}

//Defining the system

double error_function(double x){
double dim=1;
gsl_odeiv2_system F;
F.function= error_diff_function;
F.dimension= dim;
F.jacobian= NULL;
F.params= NULL;


// Allocating the system and telling the system how to solve the system and what accuracy and stepsize to use.
double acc=1e-6, eps=1e-6;
double stepsize=copysign(0.1,x);

gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(&F, gsl_odeiv2_step_rkf45, stepsize, acc, eps);

double t=0,y[1]={0}; // solving the system using following initial condition
gsl_odeiv2_driver_apply(driver, &t, x,y);
gsl_odeiv2_driver_free(driver);
return y[0];
}

int main(int argc, char** argv) {
double a=atof(argv[1]);
double b=atof(argv[2]);
double dx=atof(argv[3]);

for (double x=a; x <=b; x+=dx)printf("%g %g %g\n",x,error_function(x), erf(x));
  return 0;
}
