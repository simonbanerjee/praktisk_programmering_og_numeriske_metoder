#include "stdio.h"
#include "limits.h"
#include "float.h"

int main()
{

/* Check INT_MAX for while, for and do while loops*/

	
/* This for the while loop */
printf("Opgave 1  \n");

printf("opgave i) \n");



printf("The maximum integer using INT_MAX is = %d\n", INT_MAX );

int i=1; 
while (i+1 > i) {i++;}
printf("My maximum integer using while loop is = %d\n",i );

/* This for the for loop*/

int j;
for (j=1; j+1>j;j++){}
	printf("My maximum integer using for loop is = %d\n",j );

/* This for the for loop*/

int k=1;
do 
{k++;}
while(k+1>k);
printf("My maximum integer using do while loop= %d\n\n",k); 


/*ii)  Nu gøres det samme for INT_MIN*/
printf("Opgave ii)  \n");

printf("The minimum integer using INT_MIN is = %d\n", INT_MIN );

	int a=1; 
while (a-1 < a) {a--;}
printf("My minimum integer using while loop is = %d\n",a );

/* This for the for loop*/

int b;
for (b=1; b-1<b;b--){}
	printf("My minimum integer using for loop is = %d\n",b );

/* This for the for loop*/

int c=1;
do 
{c--;}
while(c-1<c);
printf("My minimum integer using do while loop= %d\n\n",c); 


// opgave iii) The machine epsilon is the difference between 1.0 and the next representable floating point number. 
//Using the while loop calculate the machine epsilon for types float, double, and long double,
// and compare with the values FLT_EPSILON, DBL_EPSILON, and LDBL_EPSILON defined in float.h.

printf("Opgave iii)  \n");

printf("FLT_EPSILON is %g\n",FLT_EPSILON);
printf("DBL_EPSILON is  %g\n",DBL_EPSILON);
printf("LDBL_EPSILON %Lg\n\n", LDBL_EPSILON);


float fe_while= 1;
while(1+fe_while != 1){fe_while/=2;} fe_while*=2;
printf("float epsilon using while is = %g\n",fe_while);

double de_while = 1;
while(1+de_while!=1) {de_while/=2;} de_while*=2;
printf("double epsilon using while = %g\n",de_while);

long double le_while = 1.0;
while(1+le_while!=1) {le_while/=2;} le_while*=2;
printf("long epsilon using while = %Lg\n\n",le_while);



float fe_for=1;
for (fe_for; 1+fe_for!=1; fe_for/=2){} fe_for*=2;
	printf("float epsilon using for is = %g\n",fe_for);

double de_for=1;
for (de_for; 1+de_for!=1; de_for/=2){} de_for*=2;
	printf("double epsilon using for is = %g\n",de_for);

long double le_for=1;
for (le_for; 1+le_for!=1; le_for/=2){} le_for*=2;
	printf("long epsilon using for is = %Lg\n\n",le_for);




float fe_do=1;
do
{fe_do/=2;}
while(1+fe_do!=1); 

printf("float epsilon for do while= %g\n",fe_do*=2); 



double de_do=1;
do
{de_do/=2;}
while(1+de_do!=1); 

printf("double epsilon for do while= %g\n",de_do*=2); 


long double le_do=1;
do
{le_do/=2;}
while(1+le_do!=1); 

printf("long epsilon for do while= %Lg\n",le_do*=2); 









	return 0;
}