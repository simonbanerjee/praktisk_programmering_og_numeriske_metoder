#include <stdio.h>
#include <math.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_vector.h>
#include <math.h>


double rosenbrock(const gsl_vector* x, void * params){
double x1=gsl_vector_get(x,0);
double x2=gsl_vector_get(x,1);

double rosenbrock=pow((1-x1),2) + 100*pow((x2-x1*x1),2); // Defining the Rosenbrock function
return rosenbrock;
}

int main(){
int dim=2;
gsl_multimin_function F; // defining the funciton
F.f=rosenbrock;
F.n=dim;
F.params=NULL;

gsl_vector *step = gsl_vector_alloc(dim); //allocate memory for the function
gsl_vector_set_all(step,0.01); //defining the step size for all variabels

gsl_vector * start=gsl_vector_alloc(dim); //allocate memory for the funciton
gsl_vector_set(start,0,3); // x start
gsl_vector_set(start,1,3); // y start

gsl_multimin_fminimizer *state =gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,dim); //Allocate memory
gsl_multimin_fminimizer_set(state, &F, start, step); //initializes minimizer

// When doing the minimization: The functiongsl_multimin_fminimizer_iterate do one iteration of the minimizer to update the state of the minimizer
int iter=0;
int status;
do{iter++;int iteration_status = gsl_multimin_fminimizer_iterate(state);if(iteration_status != 0){
	fprintf(stderr,"unable to improve \n");
	break;}

	double acc=0.001;
	status = gsl_multimin_test_size(state->size, acc); // Testing the minimizer specific characteristic size against the accurecy defind. GSL_SUCESS is returned if the size is smaller that the tolerance otherwise GSL_CONTINUE is returned
	if( status == GSL_SUCCESS ) // If GSL:SUCESS is returned by the testing function the results is printed out
	printf("iter=%i \t x= %f \t y= %f \t Rosenbrock= %g \t size= %g \n",
	iter,gsl_vector_get(state->x,0),gsl_vector_get(state->x,1),state->fval,state->size); // Printing data
}while( status == GSL_CONTINUE && iter < 100);



// Freeing memory
gsl_multimin_fminimizer_free(state);
gsl_vector_free(start);
gsl_vector_free(step);



  return 0;
}
