#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<math.h>
// The program writes everything into text files aswell as the terminal

typedef struct {int n; double *t,*y,*e;} exp_data;// defines a structure which takes the input n, t, y and e calls it exp_data

double decay_fun (const gsl_vector *x, void *params){
double  A = gsl_vector_get(x,0);
double  T = gsl_vector_get(x,1);
double  B = gsl_vector_get(x,2);

exp_data *p = (exp_data*) params;

int     n = (*p).n; // Unpacking values to be used in the minimization sum
double *t = (*p).t;
double *y = (*p).y;
double *e = (*p).e;
double sum=0;
#define f(t) A*exp(-(t)/T) + B
for(int i=0;i<n;i++){sum+= pow( (f(t[i]) - y[i])/e[i],2);}// The sum which needs to be minimized to obtain the best correspondance between experimental data and theory
return sum;
}

int main(){
double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};// Experimental data
double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
int n = sizeof(t)/sizeof(t[0]); // Size of data vectors (number of entries)


exp_data params; // Writing the experimental data into params of the type exp_data
	params.n=n;
	params.t=t;
	params.y=y;
	params.e=e;


gsl_multimin_function F; // Defining the system
	F.f=decay_fun;
	F.n=3;
	F.params=(void*)&params;


gsl_vector *start = gsl_vector_alloc(F.n);// Allocating a vector for a system of 3 dimensions
	gsl_vector_set(start,0,3);// Setting the initial values. Chosen to be 3 for all.
	gsl_vector_set(start,1,3);
	gsl_vector_set(start,2,3);

	gsl_vector *step = gsl_vector_alloc(F.n); // Allocating a vector which will contain the step size
	gsl_vector_set_all(step,0.2);// Choosing the stepsize and inserting this value into all enteries


gsl_multimin_fminimizer *state =gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,F.n); // Allocating memory for a minimization of F.n dimensions using the method gsl_multimin_fminimizer_nmsimplex2
gsl_multimin_fminimizer_set (state, &F, start, step);//Initializes the minimizer state to minimize the function F, starting from start with step sizes step.

int iter=0, status;
do{iter++;int iteration_status = gsl_multimin_fminimizer_iterate(state); // This function preforms a single minimization iteration. It maintains a best estemate of the minimum at all time. Using a loop in order to iterate untill the wanted precision is obtained.
if(iteration_status != 0){fprintf(stderr,"unable to improve\n");break;}

double acc=0.01;
status = gsl_multimin_test_size(state->size, acc); // Using a test function to see whether the minimization has reached the wanted accuracy if yes it returns GSL_SUCESS and if no GSL_CONTINUE
if( status == GSL_SUCCESS ) fprintf(stderr,"converged\n");
	fprintf(stderr,"iter= %i  ",iter);
	fprintf(stderr,"A= %.3g  ",gsl_vector_get((*state).x,0));
	fprintf(stderr,"T= %.3g  ",gsl_vector_get((*state).x,1));
	fprintf(stderr,"B= %.3g  ",gsl_vector_get((*state).x,2));
	fprintf(stderr,"size= %.3g  ",(*state).size);
	fprintf(stderr,"\n");
}while( status == GSL_CONTINUE && iter < 100);

double A=gsl_vector_get((*state).x,0);
double T=gsl_vector_get((*state).x,1);
double B=gsl_vector_get((*state).x,2);

printf(" t \t  A*exp(-t/T)+B\n");
double dt=(t[n-1]-t[0])/50;
for(double ti=t[0]; ti<t[n-1]+dt; ti+=dt) printf("%g %g\n",ti,A*exp(-ti/T)+B);


// Freeing memory
gsl_multimin_fminimizer_free(state);
gsl_vector_free(start);
gsl_vector_free(step);
return 0;
}
