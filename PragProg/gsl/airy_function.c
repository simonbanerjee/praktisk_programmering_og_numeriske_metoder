#include <stdio.h>
#include <gsl/gsl_sf_airy.h>
#include <stdlib.h>

int main() {

double Ai;
double Bi;
double x;

for(x = -15; x < 5; x+=0.1) {
  // We calculate the airy function parameter A
  Ai= gsl_sf_airy_Ai(x,GSL_PREC_DOUBLE);
  Bi= gsl_sf_airy_Bi(x,GSL_PREC_DOUBLE);

  printf("%g \t %g \t %g\n",x,Ai,Bi);


}



return 0;
}
