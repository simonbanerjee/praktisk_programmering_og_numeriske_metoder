#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>



int main(){
// We set up the system as a Ax=B system, so we define the three vectors.
//We define the size our matrix
gsl_matrix * A= gsl_matrix_alloc(3,3); // we allocate the memory for a 3x3 gsl_matrix
// Now we define the different entries of the A matrix

gsl_matrix_set(A,0,0,6.13);
gsl_matrix_set(A,0,1,-2.90);
gsl_matrix_set(A,0,2,5.86);
gsl_matrix_set(A,1,0,8.08);
gsl_matrix_set(A,1,1,-6.31);
gsl_matrix_set(A,1,2,-3.89);
gsl_matrix_set(A,2,0,-4.36);
gsl_matrix_set(A,2,1,1.00);
gsl_matrix_set(A,2,2,0.19);


// Now we define the b-vector
gsl_vector* b= gsl_vector_alloc(3); // we allocate the memory for a 1x3 gsl vector
gsl_vector_set(b,0,6.23);
gsl_vector_set(b,1,5.37);
gsl_vector_set(b,2,2.29);

gsl_vector* x=gsl_vector_alloc(3); // This vector will contain the solutions to the linear system




// We copy the data from A into a new matrix called A_result, this is done, since
// gsl_linalg_HH_solve(A,b,x) solves x, it doesn't modify b, but destroys A, which is
// why we make a copy of A to use late.
gsl_matrix* A_result= gsl_matrix_alloc(3,3);
gsl_matrix_memcpy (A_result, A);
gsl_linalg_HH_solve(A,b,x); //this solves the linear system

printf("x=\n");
gsl_vector_fprintf(stdout,x,"%g"); // the gsl function prints the result.




gsl_vector* b_result= gsl_vector_alloc(3); // we allocate the memory for a 1x3 gsl vector
gsl_vector_set(b_result,0,0);
gsl_vector_set(b_result,1,0);
gsl_vector_set(b_result,2,0);

printf("\n Checking that A*x equals b\n");
gsl_blas_dgemv(CblasNoTrans,1.0, A_result, x,0.0, b_result);
printf("\nb_result=\n");
gsl_vector_fprintf(stdout,b_result,"%g");


// REMEMBER TO FREE ALLOCATED MEMORY
gsl_matrix_free(A);
gsl_vector_free(x);
gsl_vector_free(b);
gsl_matrix_free(A_result);
gsl_vector_free(b_result);





  return 0;
}
