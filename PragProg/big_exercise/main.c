#include <stdio.h>
#include <math.h>
#include "gsl/gsl_math.h"
#include "gsl/gsl_eigen.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"

void eigenval(double x){
// We define the dimension of the gsl_matrix
int dim=2;
gsl_matrix* A=gsl_matrix_alloc(dim,dim);

// Now I set the entries in my matrix A
gsl_matrix_set(A,0,0,x);
gsl_matrix_set(A,0,1,1);
gsl_matrix_set(A,1,0,1);
gsl_matrix_set(A,1,1,-x);

gsl_vector *eval=gsl_vector_alloc(dim);
gsl_eigen_symm_workspace * workspace=gsl_eigen_symm_alloc(dim);
gsl_eigen_symm(A,eval,workspace); // This solves the system and finds the eigenvalues, the last two terms are the exact solution
printf("%g %g %g %g %g\n", x, gsl_vector_get(eval,0), gsl_vector_get(eval,1), sqrt(pow(x,2)+1) , -sqrt(pow(x,2)+1)    );

gsl_matrix_free(A);
gsl_vector_free(eval);
gsl_eigen_symm_free(workspace);
}

int main(void) {

//we choose a x_max and a step size
double x_max=1000;
double dx=1;
double x;

printf("x \t calc1 \tcalc2 \t exact1 \t exact2 \n");

for (x=0; x < x_max ; x+=dx){
eigenval(x);

}


  return 0;
}
