#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_integration.h>

//First we define the integrand in hint a)
double norm_integrand_a(double x, void* params){
double a= *(double *) params;
  return exp(-a*x*x);
}

// Now we define the integrand in question b)
double norm_integrand_b(double x, void* params){
double a=*(double *) params;
return (-a*a*x*x/2.0 + a/2.0 + x*x/2.0)*exp(-a*x*x);// Innerproduct between gaussian functions and the hamiltonian -1/2(d²/dx²)+1/2x²
}

// Now we Define our main function
int main(){
double a;
double acc=1e-6,eps=1e-6; // We set the error

// Now we define norm_integrand_a and norm_integrand_b functions
double result_norm_a;
double err_norm_a;
gsl_function norm_a; // norm_a function defined
norm_a.function = &norm_integrand_a;

// Do same for the other function
double result_norm_b;
double err_norm_b;
gsl_function norm_b; // norm_b function defined
norm_b.function = &norm_integrand_b;

// Now we need to allocate the both integrals

//First we decide on the amount of interations
size_t limit = 1000;
gsl_integration_workspace * w_a = gsl_integration_workspace_alloc(limit);
gsl_integration_workspace * w_b =gsl_integration_workspace_alloc(limit);

// Now we make a for-loop to solve it for different values of a
for (a= 0.05; a<5; a+=0.05){
  norm_a.params=&a;
  norm_b.params=&a;

gsl_integration_qagi(&norm_a,acc,eps,limit,w_a,&result_norm_a,&err_norm_a); // This integrates the function from -inf to inf
gsl_integration_qagi(&norm_b,acc,eps,limit,w_b,&result_norm_b,&err_norm_b); // This integrates the function from -inf to inf
double result=result_norm_b/result_norm_a;
printf("%g \t %g\n",a,result);
}

//Freeing allocated workspace
gsl_integration_workspace_free(w_a);
gsl_integration_workspace_free(w_b);
return 0;
}
