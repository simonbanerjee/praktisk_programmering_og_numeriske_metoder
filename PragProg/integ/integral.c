#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double integrand(double x, void* params){
return log(x)/sqrt(x); // This is the expression we want to integrate
}

// Now we define funciton that can calculate the integration

int main() {
// define the gsl_function
gsl_function f;
//defining what the gsl_function should take as input
f.function = integrand;
f.params = NULL; // no parameters

// Defining the number of iterations
size_t limit =100;

// Now we define the integration limits and absolute and relative error
double a=0;
double b=1;
double acc=1e-6;
double eps = 1e-6;
double result;
double err;

// now we allocate the workspace
gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(limit);

// Now all the work is done, and we can now send our function to the gsl_integration_qags funciton
double integration=gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result,&err);
//Freeing the allocated space
gsl_integration_workspace_free(workspace);
if(integration!=GSL_SUCCESS){fprintf(stderr, "Integration failed\n");} // If sentence in case the integration fails
else printf("\n The integral of ln(x)/sqrt(x) from 0 to 1 is equal to %g \n", result);
// if the sentence is a succes then result is presented


  return 0;
}
