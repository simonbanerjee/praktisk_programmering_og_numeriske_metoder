#include <stdio.h>
#include <tgmath.h>
#include "stdlib.h"
#include "nvector.h"
#include <limits.h>
#define RND (double)rand()/RAND_MAX

int main() {
// Test of nvector_alloc. If failes "Test failed" is printed otherwise "Test successfull"
printf("\nChecking the nvector_alloc function \n");
double n=42; // because it's the answer to everything
nvector* w=nvector_alloc(n);
if (w==NULL)
printf("\nTest failed\n");
else printf("Test successful\n\n");

// Testing the nvector_get and nvector_free
printf("Checking if nvector_get and nvector_free works.\nSo we insert a value into the i'th entrance using nvector_set \n extracting it using nvector_get and lastly using the equal funciton to check if it is the same value. \n\n" );
double m=3.4;
int i= 1;
nvector_set(w,i,m);
double value=nvector_get(w,i);
// Now using the function equal from lecture 3 to check whether the values are identical
equal(m,value,1e-6,1e-6);

// Now we generate vectors to if nvector_dot_product works
int r=rand();
int size=10;

nvector* array_1_mem=nvector_alloc(size);
nvector* array_2_mem=nvector_alloc(size);

for (int i = 0; i < size ; i++) {
  nvector_set(array_1_mem,i,i);
};
for (int j = 0; j < size; j++) {
  nvector_set(array_2_mem,j,j);
};


printf("\nmain: testing nvector_add ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}
	nvector_add(a, b);
	nvector_print("What a+b should give = ", c);
	printf("\n");
	nvector_print("What a+b actually give =", a);




// Checking the dot product function
double result=nvector_dot_product(array_1_mem,array_2_mem);
printf("\n Testing if the dot product function works\n" );
printf("Dot product=%g\n", result );

//when using malloc function, remember to free the allocations using free
nvector_free(w);
nvector_free(array_1_mem);
nvector_free(array_2_mem);
nvector_free(a);
nvector_free(b);
nvector_free(c);




  return 0;
}
