#include <stdio.h>
#include "nvector.h"
#include <stdlib.h>
#include <math.h>
#include <assert.h>

// Function which allocated memory for a vector of size z.
nvector* nvector_alloc(int n){nvector* v = malloc(sizeof(nvector));
(*v).size = n;
(*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n"); // Checking for errors
  return v;
}


void nvector_print(char *s, nvector * v)
{
	printf("%s", s);
	for (int i = 0; i < v->size; i++)
		printf("%9.3g ", v->data[i]);
	printf("\n");
}

void nvector_free(nvector* v){free(v);} //Frees memory allocated by nvector_alloc

void nvector_set(nvector* v, int i , double m){ (*v).data[i]=m;} // taking the i'th enterance in the vector v equal to m

double nvector_get(nvector* v, int i){return v->data[i];} // Function that return the i'th enterance of a vector v

void nvector_add(nvector* a, nvector* b){
for(int i=0; i<(*a).size; i++){
(*a).data[i]+=(*b).data[i];
}
return 0;
	
}




//Construction a function which takes the dot product of two vectors.
//Make an if else to check wether the vectors are of equal length, if not error. 

double nvector_dot_product (nvector* array_1, nvector* array_2){
double dot=0; 

if ((*array_1).size==(*array_2) .size){

for(int i=0; i<(*array_1).size; i++){
dot+= nvector_get(array_1, i)*nvector_get(array_2, i);
}
return dot;
}
else{fprintf( stderr,"Arrays not equal length");}
};

// Function equal: Checking if values are approximatly if absolute or relative precision is fullfiled it return 1 else 0
int equal(double a, double b, double tau, double epsilon){

if (abs(a-b)<tau){
printf("If enterance and known value are within absolute precision or relative precision return 1 else 0\n  ");
printf("1 test successful\n");}

else if(abs(a-b)/(abs(a)+abs(b))<epsilon){
printf("If enterance and known value are within absolute precision or relative precision return 1 else 0\n  ");

printf("1 test successful \n");}

else
{printf("If enterance and known value are within absolute precision or relative precision return 1 else 0\n  ");
printf("0 test failed\n \n");}

}

// men skal du ikke definere størrelsen af TAU og epsilon?
