#ifndef HAVE_NVECTOR_H

typedef struct {int size; double* data;} nvector;

// Declarations of functions
nvector* nvector_alloc       (int n); // Function which allocates memory for a vector of size n
void     nvector_free        (nvector* v); // Freeing memory allocated
void     nvector_set         (nvector* v, int i, double value);// Function which sets the i'th enterance in the vector v equal to value
double   nvector_get         (nvector* v, int i); // Returns the i'th enterance in the vector v
double   nvector_dot_product (nvector* array_1, nvector* array_2);// Return the dot product between array_1 and array_2
int      equal(double a, double b, double tau, double epsilon); // Function which will be used to chech if nvector_set and nvector_get works
#define HAVE_NVECTOR_H
#endif
