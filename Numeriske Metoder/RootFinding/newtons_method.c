#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"newton.h"

int newton(void f(gsl_vector *x, gsl_vector *fx),gsl_vector *x, double dx, double eps)
{

//size of vector and matrix defined
int n = (*x).size;

gsl_vector * fx=gsl_vector_alloc(n);
gsl_vector * df=gsl_vector_alloc(n);
gsl_vector * xx=gsl_vector_alloc(n);
gsl_vector * fxx=gsl_vector_alloc(n);
gsl_matrix * J=gsl_matrix_alloc(n,n);
gsl_matrix * R=gsl_matrix_alloc(n,n);
gsl_vector * Dx=gsl_vector_alloc(n);// vector which will contain coefficients

int calls = 0;

do{f(x,fx); calls++;
  for(int j=0;j<n;j++){// Numerical Jacobian matrix
  gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
  f(x,df);
  gsl_vector_sub(df,fx); // f(x+dx)-f(x)
          for(int i=0;i<n;i++){
    gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);
    }
  gsl_vector_set(x,j,gsl_vector_get(x,j)-dx);
  }
// Now I have an equation on the form J Dx=-f(x) which is solved for Dx
  qr_gs_decomp(J,R);
  gsl_vector_scale(fx,-1.0);
  qr_gs_solve(J,R,fx,Dx);
  double lambda=1.0;
  do{
    lambda/=2.0;
    gsl_vector_memcpy(xx,x);
              gsl_blas_daxpy (lambda,Dx,xx);
                      f(xx,fxx);

  }while(gsl_blas_dnrm2(fxx)>(1.0-lambda/2.0)*gsl_blas_dnrm2(fx) && lambda>0.02);


   gsl_vector_memcpy(x,xx);
   gsl_vector_memcpy(fx,fxx);


}while(gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(fx)>eps);

gsl_vector_free(fx);
gsl_vector_free(df);
gsl_vector_free(Dx);
gsl_vector_free(xx);
gsl_vector_free(fxx);
gsl_matrix_free(R);
gsl_matrix_free(J);

return calls;


}

int newton_with_jacobian(void f(gsl_vector * x, gsl_vector * fx),void jacobian(gsl_vector * x, gsl_matrix * J),gsl_vector * x,double eps)
{
  int n = (*x).size;
  gsl_vector * fx=gsl_vector_alloc(n);
  gsl_vector * xx=gsl_vector_alloc(n);
  gsl_vector * fxx=gsl_vector_alloc(n);
  gsl_matrix * R=gsl_matrix_alloc(n,n);
  gsl_matrix * J=gsl_matrix_alloc(n,n);
  gsl_vector * Dx=gsl_vector_alloc(n);// vector which will contain coefficients

int calls = 0;

do{ f(x,fx); jacobian(x,J); calls++;
  // Now I have an equation on the form J Dx=-f(x) which is solved for Dx
    qr_gs_decomp(J,R);
    gsl_vector_scale(fx,-1.0);
    qr_gs_solve(J,R,fx,Dx);
    double lambda=1.0;
    do{
      lambda/=2.0;
      gsl_vector_memcpy(xx,x);
                gsl_blas_daxpy (lambda,Dx,xx);
                        f(xx,fxx);

    }while(gsl_blas_dnrm2(fxx)>(1.0-lambda/2.0)*gsl_blas_dnrm2(fx) && lambda>0.02);


     gsl_vector_memcpy(x,xx);
     gsl_vector_memcpy(fx,fxx);

}while(gsl_blas_dnrm2(fx)>eps);


gsl_vector_free(fx);
gsl_vector_free(Dx);
gsl_vector_free(xx);
gsl_vector_free(fxx);
gsl_matrix_free(R);
gsl_matrix_free(J);

return calls;
}

int newton_with_jacobian_and_inter(void f(gsl_vector * x, gsl_vector * fx),void jacobian(gsl_vector * x, gsl_matrix * J),gsl_vector * x,double eps){
 int n=(*x).size;
  gsl_vector * fx=gsl_vector_alloc(n);
  gsl_vector * xx=gsl_vector_alloc(n);
  gsl_vector * fxx=gsl_vector_alloc(n);
  gsl_matrix * R=gsl_matrix_alloc(n,n);
  gsl_matrix * J=gsl_matrix_alloc(n,n);
  gsl_vector * Dx=gsl_vector_alloc(n);// vector which will contain coefficients

  int calls=0;

	do{ f(x,fx); jacobian(x,J); calls++;
  // Now I have an equation on the form J Dx=-f(x) which is solved for Dx

		qr_gs_decomp(J,R);
		gsl_vector_scale(fx,-1.0);
		qr_gs_solve(J,R,fx,Dx);
		double lambda=1.0;
		do{
			gsl_vector_memcpy(xx,x);
            		gsl_blas_daxpy (lambda,Dx,xx);
                        f(xx,fxx);
			double g0=gsl_blas_dnrm2(fx)*gsl_blas_dnrm2(fx)/2.0;
                        double gprime0=-gsl_blas_dnrm2(fx)*gsl_blas_dnrm2(fx);
                        double gtri=gsl_blas_dnrm2(fxx)*gsl_blas_dnrm2(fxx)/2.0;
			double c=(gtri-g0-gprime0*lambda)/(lambda*lambda);
                        lambda=-gprime0/(2*c);


		}while(gsl_blas_dnrm2(fxx)>(1.0-lambda/2.0)*gsl_blas_dnrm2(fx) && lambda>0.02);


     gsl_vector_memcpy(x,xx);
     gsl_vector_memcpy(fx,fxx);


  }while(gsl_blas_dnrm2(fx)>eps);


  gsl_vector_free(fx);
  gsl_vector_free(Dx);
  gsl_vector_free(xx);
  gsl_vector_free(fxx);
  gsl_matrix_free(R);
  gsl_matrix_free(J);

  return calls;


}
