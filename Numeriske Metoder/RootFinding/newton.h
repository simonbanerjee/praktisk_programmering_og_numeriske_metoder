int newton(void f(gsl_vector *x, gsl_vector *fx),gsl_vector *x, double dx, double eps);
void qr_gs_decomp(gsl_matrix* A, gsl_matrix *R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* x);
int newton_with_jacobian(void f(gsl_vector * x, gsl_vector * fx),void jacobian(gsl_vector * x, gsl_matrix * J),gsl_vector * x,double epsilon);
int newton_with_jacobian_and_inter(void f(gsl_vector * x, gsl_vector * fx),void jacobian(gsl_vector * x, gsl_matrix * J),gsl_vector * x,double eps);
