#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"newton.h"

void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* x){
  // Solving QRx=b by Rx=Q^Tb still on the form Rx=c. using back substitution

  gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0, x);
  int m=(*x).size;
// Doing backsubstitution

  for(int i=m-1;i>=0;i--){
  double ci=0.0;
   for(int j=i+1;j<m;j++){ci-=gsl_matrix_get(R,i,j)*gsl_vector_get(x,j);}
  gsl_vector_set(x,i,(gsl_vector_get(x,i)+ci)/gsl_matrix_get(R,i,i));
  }
}
