#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"newton.h"

int print_vector(gsl_vector * M){// Function that prints vector
int n=(*M).size;
  for (int row=0; row<n; row++)printf("%10.3f     ", gsl_vector_get(M,row));
return 0;
}

// ----------------------------------------------------------------------------
void f(gsl_vector* x, gsl_vector *fx){
double A = 10000.0;
double x1 = gsl_vector_get(x,0);
double x2= gsl_vector_get(x,1);
gsl_vector_set(fx,0,A*x1*x2-1.0);
gsl_vector_set(fx,1,exp(-x1)+exp(-x2)-1.0-1.0/A);
}

void f_Jacobi(gsl_vector* x, gsl_matrix*J){
double x1 = gsl_vector_get(x,0);
double x2= gsl_vector_get(x,1);
gsl_matrix_set(J,0,0,10000.0*x2),  gsl_matrix_set(J,0,1,10000.0*x1);
gsl_matrix_set(J,1,0,-exp(-x1)),  gsl_matrix_set(J,1,1,-exp(-x2));
}


// ----------------------------------------------------------------------------

void Rosenbrock(gsl_vector * x, gsl_vector *fx){
   double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
   gsl_vector_set(fx,0,-2*(1-x1)+200*(x2-x1*x1)*(-2*x1));

   gsl_vector_set(fx,1,200*(x2-x1*x1));
   }

void Rosenbrock_jacobi(gsl_vector * x, gsl_matrix * J){
  double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
  gsl_matrix_set(J,0,0,1200*x1*x1-400*x2+2),  gsl_matrix_set(J,0,1,-400*x1);
  gsl_matrix_set(J,1,0,-400*x1),  gsl_matrix_set(J,1,1,200);
}

// ----------------------------------------------------------------------------

void Himmelblau(gsl_vector * x, gsl_vector * fx){
   double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
   gsl_vector_set(fx,0,2*(x1*x1+x2-11)*2*x1+2*(x1+x2*x2-7));

   gsl_vector_set(fx,1,2*(x1*x1+x2-11)+2*(x1+x2*x2-7)*2*x2);
   }

void  Himmelblau_jacobi(gsl_vector * x, gsl_matrix * J){
    double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
    gsl_matrix_set(J,0,0,-42+12*x1*x1 + 4*x2),  gsl_matrix_set(J,0,1,4*(x1+x2));
    gsl_matrix_set(J,1,0,4*(x1+x2)),  gsl_matrix_set(J,1,1,4*x1+12*x2*x2-26);
}

// ----------------------------------------------------------------------------


int main(){
gsl_vector *x=gsl_vector_alloc(2);

// System of equations-----------------------------------------
  // Startiong point
  double start_x11=2, start_x21=5, eps=10e-5;
  gsl_vector_set(x,0,start_x11); gsl_vector_set(x,1,start_x21);

  int calls1=newton_with_jacobian_and_inter(f,f_Jacobi,x,eps);
  printf("Root for the system of equations (x,y)=");
  print_vector(x);
  printf("\n Found calling the function %i times",calls1);

// Rosenbrock
  double start_x12=7, start_x22=2;
  gsl_vector_set(x,0,start_x12); gsl_vector_set(x,1,start_x22);
  int calls2=newton_with_jacobian_and_inter(Rosenbrock,Rosenbrock_jacobi,x,eps);
  printf("\n \nRoot for the Rosenbrock function (x,y)=");
  print_vector(x);
  printf("\n Found calling the function %i times",calls2);

// Himmelblau
  double start_x13=2, start_x23=2;
  gsl_vector_set(x,0,start_x13); gsl_vector_set(x,1,start_x23);
  int calls3=newton_with_jacobian_and_inter(Himmelblau,Himmelblau_jacobi,x,eps);
  printf("\n \nRoot for the Himmelblau function (x,y)=");
  print_vector(x);
  printf("\n Found calling the function %i times",calls3);

//Compare the number of steps and the number of function calls for the refined linear search and the simple backtracking.
// This is done with the same initial values

printf("\n \nComparison between refined linear search and the simple backtracking for the 3 functions using the same initial x-values\n");

double dx=0.0001;
gsl_vector_set(x,0,start_x11); gsl_vector_set(x,1,start_x21);
int calls_back=newton(f,x,dx,eps);
printf("\nFor the first set of equations the simple backtracking is called %i times and the refined linear search is called %i\n",calls_back,calls1);


gsl_vector_set(x,0,start_x12); gsl_vector_set(x,1,start_x22);
calls_back=newton(Rosenbrock,x,dx,eps);
printf("\nFor the Rosenbrock function the simple backtracking is called %i times and the refined linear search is called %i \n ",calls_back,calls2);

gsl_vector_set(x,0,start_x13); gsl_vector_set(x,1,start_x23);
calls_back=newton(Himmelblau,x,dx,eps);
printf("\nFor the Himmelblau function the simple backtracking is called %i times and the refined linear search is called %i\n",calls_back,calls3);









  return 0;
}
