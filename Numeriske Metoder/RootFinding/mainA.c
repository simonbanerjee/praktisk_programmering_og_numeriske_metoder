#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"newton.h"

int print_vector(gsl_vector * M){// Function that prints vector
int n=(*M).size;
  for (int row=0; row<n; row++)printf("%10.3f     ", gsl_vector_get(M,row));
return 0;
}

void f(gsl_vector* x, gsl_vector *fx){
double A = 10000.0;
double x1 = gsl_vector_get(x,0);
double x2= gsl_vector_get(x,1);
gsl_vector_set(fx,0,A*x1*x2-1.0);
gsl_vector_set(fx,1,exp(-x1)+exp(-x2)-1.0-1.0/A);
}

void Rosenbrock( gsl_vector *x, gsl_vector *fx) {
  double x1 = gsl_vector_get(x,0);
  double x2= gsl_vector_get(x,1);
  gsl_vector_set(fx,0, 2*(1-x1)*(-1)+100*2*(x2-x1*x1)*(-1)*2*x1);
	gsl_vector_set(fx,1, 100*2*(x2-x1*x1));
}

void Himmelblau(gsl_vector * x, gsl_vector * fx){
   double x1=gsl_vector_get(x,0);
   double x2=gsl_vector_get(x,1);
   gsl_vector_set(fx,0,2*(x1*x1+x2-11)*2*x1+2*(x1+x2*x2-7));
   gsl_vector_set(fx,1,2*(x1*x1+x2-11)+2*(x1+x2*x2-7)*2*x2);
   }

int main() {
gsl_vector *x =gsl_vector_alloc(2);

//my system of equations -------------------------------------------------------

// I set some starting points
double x_start= 2;
double y_start= 4;
double dx=0.0001;
double eps= 10e-5;

gsl_vector_set(x,0,x_start);
gsl_vector_set(x,1,y_start);

int calls=newton(f,x,dx,eps);
printf("The root of the system (x,y)=\n");
print_vector(x);
printf("\nFound calling the function %i times\n",calls);

// Rosenbrock Function---------------------------------------------------------
x_start= 2;
y_start= 2;
dx=0.0001;
eps= 10e-5;

gsl_vector_set(x,0,x_start);
gsl_vector_set(x,1,y_start);

calls=newton(Rosenbrock,x,dx,eps);
printf("\nThe root of the Rosenbrock function (x,y)= \n");
print_vector(x);
printf("\nFound calling the function %i times\n",calls);

//Himmelblau function ----------------------------------------------------------
x_start= -2;
y_start= 2;
dx=0.0001;
eps= 10e-5;

gsl_vector_set(x,0,x_start);
gsl_vector_set(x,1,y_start);

calls=newton(Himmelblau,x,dx,eps);
printf("\nThe root of the Himmelblau function (x,y)= \n");
print_vector(x);
printf("\nFound calling the function %i times\n\n",calls);
printf("\nThe Himmelblau function have four roots. At \n");
printf("\n (3.0,2.0)=0.0 \n");
printf("\n (-2.805118,3.131312)=0.0 \n");
printf("\n (-3.779310,-3.283186)=0.0 \n");
printf("\n (3.584428,-1.848126)=0.0 \n");





  return 0;
}
