int print_matrix(gsl_matrix * M);
int print_vector(gsl_vector * M);
double randfrom(double min, double max);
double Newton_Raphson(double f(double x, gsl_matrix * D, gsl_vector * u), double x, double dx, double eps, gsl_matrix * D,gsl_vector * u);
gsl_matrix * outer_product(gsl_vector * v, gsl_vector * u);
