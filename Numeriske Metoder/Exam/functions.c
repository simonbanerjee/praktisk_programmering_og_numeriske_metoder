#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

// These two functions can print either a matrix or a vector

// Function that prints matrix
int print_matrix(gsl_matrix * M){
int n=(*M).size1, m=(*M).size2;
for (int row=0; row<n; row++)
{
    for(int columns=0; columns<m; columns++)
         printf("%10.3f", gsl_matrix_get(M,row,columns));
    printf("\n");
 }
return 0;
}

// Function that prints vector
int print_vector(gsl_vector * M)
{
int n=(*M).size;
  for (int row=0; row<n; row++)

         printf("%10.3f", gsl_vector_get(M,row));

return 0;
}

// Function which generates a random number in the specified range.
double randfrom(double min, double max)
{
    double range = (max - min);
    double div = RAND_MAX / range;
    return min + (rand() / div);
}


// This is the code for the Newton-Raphson function. This function is used to find roots. The function is rewritten from the one during the course.
double Newton_Raphson( double f(double x, gsl_matrix * D, gsl_vector * u), double x, double dx, double eps, gsl_matrix * D, gsl_vector * u)
{
	double iter=0;
	do{
	   double df_dx=(f(x+dx,D,u)-f(x,D,u))/dx; // Numerical derivative
	   double Dx=-f(x,D,u)/df_dx; // Correction to x.
	   double lambda=1;

     while(fabs(f(x+lambda*Dx,D,u))>(1-lambda/2.0)*fabs(f(x,D,u)) && lambda>1.0/64.0){lambda/=2;}

    x+=lambda*Dx;
	  iter++;
	  if(iter>1e4){fprintf(stderr,"\n No root found. Function did not converge\n"); break;}
	 } while(fabs(f(x,D,u))>eps);
	return x;
}

// This is used to check the found eigenvalues using gsl.
gsl_matrix * outer_product(gsl_vector * v, gsl_vector * u)
{
	size_t size=(*v).size;
	gsl_matrix * A=gsl_matrix_calloc(size,size);
	gsl_matrix * B=gsl_matrix_alloc(size,size);
	gsl_matrix * C=gsl_matrix_alloc(size,size);
	for(int i=0; i<size;i++)
  {
		  for(int j=0; j<size;j++)
    {
			gsl_matrix_set(A,i,i,gsl_vector_get(v,i));
			gsl_matrix_set(B,i,j,gsl_vector_get(u,j));
		}
	}
	gsl_blas_dgemm (CblasTrans,CblasNoTrans, 1.0,A,B,0.0,C);

	return C;

gsl_matrix_free(A);
gsl_matrix_free(B);
gsl_matrix_free(C);
}
