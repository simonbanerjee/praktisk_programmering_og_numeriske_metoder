#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_sort_vector.h>
#include"functions.h"
#include<time.h>
#include <gsl/gsl_eigen.h>

// Function for the secular equation
double secular_equation(double x, gsl_matrix * D ,gsl_vector * u)
{
			double u_i, d_i, func=1;
			for(int i=0;i<(*u).size;i++){
			u_i=gsl_vector_get(u,i);
			d_i=gsl_matrix_get(D,i,i);
			func+=u_i*u_i/(d_i-x);
			}
		 	return func;
}

int main()
{
  srand((unsigned)time(NULL));

	size_t n=7;// Number of lines and columns in matrices and vectors

  // Allocating space
	gsl_matrix * D=gsl_matrix_calloc(n,n);
	gsl_vector * D_vector=gsl_vector_alloc(n);
	gsl_vector * u=gsl_vector_alloc(n);
	gsl_vector * Eigenvals=gsl_vector_calloc(n);

// Now I generate a random diagonal matrix and vector
for(int i=0;i<n;i++)
{
  double xx=randfrom(-100,100.0);
  double yy=randfrom(-100,100.0);

// Setting the dioagonal
  gsl_matrix_set(D,i,i,xx);
  gsl_vector_set(D_vector,i,xx);
  gsl_vector_set(u,i,yy);
}

printf("This the random generated diagonal matrix and vector\n \n D=\n");
print_matrix(D);
printf("\n\n u^T=");
print_vector(u);


// Now I use my Newton-Raphson method to find the roots
double dx=1e-10, eps=1e-3, lam, x;
gsl_sort_vector (D_vector);
for(int i=0;i<n;i++)
{
    if(i<(n-1))
    { // Initial guess.
      x=randfrom(gsl_vector_get(D_vector,i),gsl_vector_get(D_vector,i+1));
      lam=Newton_Raphson(secular_equation,x,dx,eps,D,u);
    }
    else
    {
      x=gsl_vector_get(D_vector,i)+5;// The largest eigenvalue is larger than the largest diagonal element.
      lam=Newton_Raphson(secular_equation,x,dx,eps,D,u);
    }
    gsl_vector_set(Eigenvals,i,lam);
}
printf("\n\n\n The following eigenvalues are obtained, by using the Newton-Raphson to find the roots of the secular equation.\n");
print_vector(Eigenvals);

// Lastly I check the found eigenvalues using gsl

// Generating the matrix A For the gsl eigenvalue function
gsl_matrix_add (D,outer_product(u,u));
printf("\n\n\nTo check the eigenvalues found using the secular equation, I use the gsl library");
printf("\nA=D+uu^T=\n");
print_matrix(D);


gsl_vector *eval = gsl_vector_alloc (n);
gsl_matrix *evec = gsl_matrix_alloc (n, n);
gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc (n);// Allocating workspace

  gsl_eigen_symmv (D, eval, evec, w);// Computing eigenvalues and eigenvectors
  printf("\nEigenvalues computed using gsl\n");
  gsl_sort_vector (eval);
print_vector(eval);
printf("\n");

gsl_matrix_free(D);
gsl_vector_free(D_vector);
gsl_vector_free(u);
gsl_vector_free(Eigenvals);
gsl_eigen_symmv_free (w);


  return 0;
}
