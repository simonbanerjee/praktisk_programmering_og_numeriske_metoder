#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include"ODE.h"
int ode_driver(void f(int n, double x, double *y, double * dydx), int n, double *yvector, double t, double b, double h, double acc, double eps, int max)
{
	int i, k=0; double  x, *y, s, tau ,err, normy, tol, yh[n], dy[n], a=t;

	while(t<b){
		x=t, y=yvector;

		if(x+h>b){h=b-x;}
		rkstep12(f,n,x,y,h,yh,dy);// Stepper
		s=0; for(i=0;i<n;i++){s+=dy[i]*dy[i]; err=sqrt(s);}// Error estimate

		tau=0; for(i=0;i<n;i++){tau+=yh[i]*yh[i]; normy=sqrt(tau);}
		tol=(normy*eps+acc)*sqrt(h/(b-a));// Tolerance

		if(err<tol){ //Rejecting step if local error is greater than local tolerance

			t=x+h; for(i=0;i<n;i++){yvector[i]=yh[i];}

		}
		if(err>0){h*=pow(tol/err,0.25)*0.95;}// New step according to equation 40 in the notes
		else h*=2;
	k++;
	}

}


int ode_driver_soring_path(void f(int n, double x, double *y, double * dydx), int n, double *xvector, double **ymatrix, double b, double h, double acc, double eps, int max){
	int i, k=0; double  x, *y, s, tau ,err, normy, tol, a=xvector[0], yh[n], dy[n];

	while(xvector[k]<b){
		x=xvector[k], y=ymatrix[k];

		if(x+h>b){h=b-x;}
		rkstep12(f,n,x,y,h,yh,dy);// Stepper

		s=0; for(i=0;i<n;i++){s+=dy[i]*dy[i]; err=sqrt(s);}// Error estimate

		tau=0; for(i=0;i<n;i++){tau+=yh[i]*yh[i]; normy=sqrt(tau);}
		tol=(normy*eps+acc)*sqrt(h/(b-a));// Tolerance

		if(err<tol){ //Rejecting step if local error is greater than local tolerance
			k++; if(k>max-1){return -k;}
			xvector[k]=x+h; for(i=0;i<n;i++){ymatrix[k][i]=yh[i];}
		}
		if(err>0){h*=pow(tol/err,0.25)*0.95;}// New step according to equation 40 in the notes
		else h*=2;
	}

	return k+1;

}

int driver_path(
	gsl_matrix* path,
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double t, double h, gsl_vector*yt,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*yth, gsl_vector*err
		),
	void f(double t, gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
){
/*	Define things	*/
	int n = path->size2-1, max_steps = path->size1, step = 0;
	double tolerance, t;
	gsl_vector* yth = gsl_vector_alloc(n);
    gsl_vector* err = gsl_vector_alloc(n);
    gsl_vector* y = gsl_vector_alloc(n);


    /*	While t_i < b 	*/
	while(gsl_matrix_get(path,step,0) < b){
	/*	Set parameters t and y	*/
		t = gsl_matrix_get(path,step,0);
		for(int i=0; i<n; i++){
		gsl_vector_set(y,i,gsl_matrix_get(path,step,i+1));
		}

		if (fabs(t + *h) > fabs(b)) *h = b - t;
		if(t>=b) break;

		stepper(t,*h,y,f,yth,err);
		tolerance = (acc+eps*gsl_blas_dnrm2(yth));

		if(tolerance > gsl_blas_dnrm2(err)){
			step++;
			if(step+1 > max_steps){
				fprintf(stderr, "\n Max number of steps \n");
				break;
			}
		/*	Set t and y values in path matrix 	*/
			gsl_matrix_set(path, step, 0, t+ *h);
			for(int i=0; i<n; i++){
				gsl_matrix_set(path, step, i+1, gsl_vector_get(yth,i));
			}
		}
		if( err > 0){
			*h = *h*pow(tolerance/gsl_blas_dnrm2(err),0.25)*0.95;
		}
		else *h=2;
	}

	gsl_vector_free(yth); gsl_vector_free(err);

	return step;
}
