#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"ODE.h"

void rkstep12(void f(int n, double x, double * yx, double * dydx),int n, double x, double * yx, double h, double * yh, double * dy){

int i;
double k0[n];
double yt[n];
double k12[n];
f(n,x,yx,k0);

for (i = 0; i < n; i++)
{
  yt[i]=yx[i]+k0[i]*h/2;
}

f(n,x+h/2,yt,k12);

for (i = 0; i < n; i++)
{
  yh[i]=yx[i]+k12[i]*h; // new function
}

for(i=0;i<n;i++)
{
  dy[i]=(k0[i]-k12[i])*h/2; //Error estimate
}

}


void rkstep2(void f(int n, double x, double * yx, double * dydx),int n, double x, double * yx, double h, double * yh, double * dy){
	int i;
  double k0[n];
  double yt[n];
  double k12[n];
	f(n,x,yx,k0);

	for(i=0;i<n;i++)
  {
    yt[i]=yx[i]+k0[i]*h/2;
  }

  f(n,x+h/2,yt,k12);

  for(i=0;i<n;i++)
  {
    yh[i]=yx[i]+k12[i]*h; // New function value
  }

  for(i=0;i<n;i++)
  {
    dy[i]=(k0[i]-k12[i])*h/2; //Error estimate
  }


}

void rkstep122(
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* yt, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* yth,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
){

	int n = yt->size;
/*	Allocate memory	*/
	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* k12 = gsl_vector_alloc(n);

	gsl_vector_memcpy(yth,yt);

	f(t,yt,k0);
/*	daxpy: y = ax + y 	*/
	gsl_blas_daxpy(h/2, k0,  yt);	/* y_i+1 = y_i + k0_i * h/2 */
	f(t+h/2, yt, k12);
	gsl_blas_daxpy(h, k12, yth);	/* y_i+1 + h[i] = y_i + k12_i * h */

/*	err[i] = (k0[i] - k12[i]) * h/2;	*/
	gsl_vector_sub(k0,k12);
	gsl_vector_scale(k0,h/2);
	gsl_vector_memcpy(err,k0);

/*	Free Allocated Memory 	*/
	gsl_vector_free(k0); gsl_vector_free(k12);
}
