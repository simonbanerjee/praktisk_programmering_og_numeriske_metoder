#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"ODE.h"
#define float double


void integrand(double x, gsl_vector * y, gsl_vector * dydx){
	gsl_vector_set(dydx,0, cos(x));	/* dydx = f(x)	*/
}

int main(){
	printf("\n C. A definite integral as an ODE\n");

	int n = 1, max_steps = 1e3;
	double a = 0, b = M_PI/2, h, abs = 1e-3, eps = 1e-3;
	gsl_matrix* path = gsl_matrix_calloc(max_steps, n+1); /* [t y1...yn]	*/

	gsl_matrix_set(path,0,0, a);	/*	initial t 	*/
	gsl_matrix_set(path,0,1, 0);	/*	y(0)=1	*/
	h = copysign(0.01,b-a);	/*	step direction dependent on sign	*/

	int steps = driver_path(path,b,&h,abs,eps,&rkstep122,integrand);
	printf("Steps = %i\n", steps);
  printf("solving y'(x)=f(x) for f(x) = cos(x)\n");
	printf("Solved I = y(pi/2) = %g\n",
		gsl_matrix_get(path,steps,1));
	printf("Exact  I = y(pi/2) = 1 \n");

	gsl_matrix_free(path);

	return 0;
}
