#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"ODE.h"
#define float double

void sin_cos(int n, double x, double* y, double *dydx)
{
	dydx[0]=y[1];
	dydx[1]=-y[0];
}

int main(){
	int n=2, max=500,i, iter;double h=1e-2, acc=1e-2, eps=1e-2;
// Part A--------------------------------------------------------------------

  double *yvector=(double*)calloc(n,sizeof(double*));
	double t=0.0, b=4*M_PI; yvector[0]=0, yvector[1]=1;


	for(double b=0.0; b<4*M_PI;b+=0.1){
		iter=0;
		iter+=iter;
		iter=ode_driver(sin_cos,n,yvector,t,b,h,acc,eps,max);
		printf("%g %g %g \n",b,yvector[0],yvector[1]);
		t=b;
	}


  // Part B--------------------------------------------------------------------
  	printf("\n \n");
  	double *xvector=(double*)calloc(max,sizeof(double));
  	double **ymatrix=(double**)calloc(max,sizeof(double*));
  	for(int i=0;i<max;i++) ymatrix[i]=(double*)calloc(n,sizeof(double));


  	double a2=0, b2=4*M_PI;
  	xvector[0]=a2; ymatrix[0][0]=0; ymatrix[0][1]=1; // Setting initial values
  	iter=ode_driver_soring_path(sin_cos,n,xvector,ymatrix,b2,h,acc,eps,max);

  	for(i=0; i<iter;i++){printf("%g %g %g\n",xvector[i],ymatrix[i][0],ymatrix[i][1]);}
  	printf("\n \n"); double x_val=0;
  	for(i=0; i<b;i++){printf("%g %g %g\n",x_val,sin(x_val),cos(x_val)); x_val++;}






  return 0;
}
