#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>






void rkstep12(void f(int n, double x, double * yx, double * dydx),int n, double x, double * yx, double h, double * yh, double * dy);
int ode_driver(void f(int n, double x, double *y, double * dydx), int n, double *yvector, double t, double b, double h, double acc, double eps, int max);
int ode_driver_soring_path(void f(int n, double x, double *y, double * dydx), int n,double *xlist, double **ylist, double b, double h, double acc, double eps, int max);
int driver_path(
	gsl_matrix* path,
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double t, double h, gsl_vector*yt,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*yth, gsl_vector*err
		),
	void f(double t, gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
);
void rkstep122(
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* yx, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* yth,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
);
