#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"qr_gs.h"
int jacobi_cyc(gsl_matrix * A, gsl_vector * e, gsl_matrix * V){
  int n=(*A).size1, changed, sweeps=0;
  gsl_matrix_set_identity(V);

  for(int i=0;i<n;i++){gsl_vector_set(e,i,gsl_matrix_get(A,i,i));}// As the loop runs the diagonal in A becomes the eigenvalues which are inserted into e (diagonal A)

  do{changed=0; int p, q;
   for(p=0; p<n; p++)for(q=p+1;q<n;q++){// Upper part
        // Finding angle that zeros element
   double A_pp=gsl_vector_get(e,p);
   double A_qq=gsl_vector_get(e,q);
   double A_pq=gsl_matrix_get(A,p,q);
   double phi=1.0/2.0*atan2(2*A_pq,A_qq-A_pp);
   double s=sin(phi), c=cos(phi);
   double A_pp1=pow(c,2)*A_pp-2*s*c*A_pq+pow(s,2)*A_qq;
   double A_qq1=pow(s,2)*A_pp+2*s*c*A_pq+pow(c,2)*A_qq;
     if(A_qq1!=A_qq || A_pp1!=A_pp){changed=1; sweeps++; // Checking wheter the diagonal elemnts has changed
      gsl_vector_set(e,p,A_pp1);
      gsl_vector_set(e,q,A_qq1);
      gsl_matrix_set(A,p,q,0.0);
      for(int i=0;i<p;i++)
			{
       double A_ip=gsl_matrix_get(A,i,p);
			 double A_iq=gsl_matrix_get(A,i,q);
       gsl_matrix_set(A,i,p,c*A_ip-s*A_iq);
       gsl_matrix_set(A,i,q,s*A_ip+c*A_iq);
      }

			for(int i=p+1;i<q;i++)
			{
       double A_pi=gsl_matrix_get(A,p,i);
			 double A_iq=gsl_matrix_get(A,i,q);
       gsl_matrix_set(A,p,i,c*A_pi-s*A_iq);
       gsl_matrix_set(A,i,q,s*A_pi+c*A_iq);
      }

			for(int i=q+1;i<n;i++)
			{
       double A_pi=gsl_matrix_get(A,p,i);
			 double A_qi=gsl_matrix_get(A,q,i);
       gsl_matrix_set(A,p,i,c*A_pi-s*A_qi);
       gsl_matrix_set(A,q,i,s*A_pi+c*A_qi);
      }

			for(int i=0;i<n;i++)
			{
       double A_ip=gsl_matrix_get(V,i,p);
			 double A_iq=gsl_matrix_get(V,i,q);
       gsl_matrix_set(V,i,p,c*A_ip-s*A_iq);
       gsl_matrix_set(V,i,q,s*A_ip+c*A_iq);
      }
     }
   }
  }while(changed!=0 && sweeps < 1000);
  return sweeps;
}
