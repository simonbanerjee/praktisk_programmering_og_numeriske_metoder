#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<time.h>
#include"qr_gs.h"

double func(int i, double x){
   switch(i){
   case 0: return log(x); break;
   case 1: return 1.0;    break;
   case 2: return x;      break;
   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}

void least_square_fit(int m, double func(int i , double x),gsl_vector *x ,gsl_vector *y , gsl_vector * dy, gsl_vector *c)
{
// We set the size of the matrix
int n=(*x).size;
gsl_matrix * A=gsl_matrix_alloc(n,m);
gsl_matrix * R= gsl_matrix_alloc(m,m);
gsl_vector * b= gsl_vector_alloc(n);

// I make a for loop to  get the x and y values

for (int i = 0; i < n; i++)
{
  double xi = gsl_vector_get(x,i);
  double yi = gsl_vector_get(y,i);
  gsl_vector_set(b,i,yi);
  for (int j = 0; j < m; j++)
  {
    gsl_matrix_set(A,i,j,func(j,xi));
  }
}

qr_gs_decomp(A,R);
qr_gs_solve(A,R,b,c);

gsl_matrix_free(A);
gsl_matrix_free(R);
gsl_vector_free(b);

}

int main()
{
  // I define my x, y and dy data, which are given in the website under the specific assignment
double  x[]  = {0.100,    1.330,    2.550,    3.780,    5.000,    6.220,    7.450,    8.680,    9.900};
double  y[]  = {-15.300,  0.320,    2.450,    2.750,    2.270,    1.350,    0.157,   -1.230,   -2.750};
double  dy[] = {1.040,    0.594,    0.983,    0.998,    1.110,    0.398,    0.535,    0.968,    0.478};

int n = sizeof(x)/sizeof(x[0]);
int m=3;

gsl_vector * xx= gsl_vector_alloc(n);
gsl_vector * yy= gsl_vector_alloc(n);
gsl_vector * dyy= gsl_vector_alloc(n);

gsl_vector * c = gsl_vector_alloc(m);

for (int i = 0; i < n; i++)
{
gsl_vector_set(xx,i,x[i]);
gsl_vector_set(yy,i,y[i]);
gsl_vector_set(dyy,i,dy[i]);
}

for (int i = 0; i < n; i++)
{
printf("%g %g %g \n", x[i],y[i],dy[i]);
}
printf("\n\n");

least_square_fit(m,func,xx,yy,dyy,c);

for(double z=0.1 ; z<=12 ; z+=0.01){
  double s=0;
for (int j = 0; j < m; j++) s+=gsl_vector_get(c,j)*func(j,z);
printf("%g %g \n",z,s);
}

gsl_vector_free(xx);
gsl_vector_free(yy);
gsl_vector_free(dyy);
gsl_vector_free(c);



  return 0;
}
