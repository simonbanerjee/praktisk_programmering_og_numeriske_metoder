#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<time.h>
#include"qr_gs.h"

// I define a function that prints my matrix
int print_matrix(gsl_matrix *M){
// I create a matrix n x m matrix where n>m
int n=(*M).size1;
int m=(*M).size2;
for (int row = 0; row< n; row++)
{
  for(int columns=0 ; columns<m ; columns++)
    printf("%10.3f", gsl_matrix_get(M,row,columns));
    printf("\n");
}
return 0;
}

// I now create a function that prints a vector

int print_vector(gsl_vector * M){// Function that prints vector
int n=(*M).size;
  for (int row=0; row<n; row++)

         printf("%10.3f     ", gsl_vector_get(M,row));

return 0;
}

double func(int i, double x){
   switch(i){
   case 0: return log(x); break;
   case 1: return 1.0;    break;
   case 2: return x;      break;
   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}

void sing_val_de(gsl_matrix *A, gsl_matrix * U, gsl_matrix * S, gsl_matrix * V )
{
// We set the size of the matrix
int n=(*A).size1;
int j=(*S).size1;

gsl_matrix * ATA=gsl_matrix_alloc(j,j);
gsl_matrix * D= gsl_matrix_alloc(j,j);
gsl_vector * e= gsl_vector_alloc(j);

// Now we generate the ATA matrix
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,ATA);
jacobi_cyc(ATA,e,V);
for(int i=0; i<j; i++)gsl_matrix_set(D,i,i,gsl_vector_get(e,i));

for(int i=0; i<j;i++){gsl_matrix_set(S,i,i,sqrt(gsl_matrix_get(D,i,i)));}

gsl_matrix * AV=gsl_matrix_calloc(n,j);
gsl_matrix * D_sqrt_inv=gsl_matrix_calloc(j,j);
gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,A,V,0.0,AV);// Generating ATA
for(int i=0; i<j;i++){gsl_matrix_set(D_sqrt_inv,i,i,1.0/sqrt(gsl_matrix_get(D,i,i)));}
gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,AV,D_sqrt_inv,0.0,U);



gsl_matrix_free(ATA);
gsl_vector_free(e);
gsl_matrix_free(D);
gsl_matrix_free(AV);

}

void sing_val_de_solve(gsl_matrix * U, gsl_matrix * S, gsl_matrix * V, gsl_vector * b, gsl_vector * c, gsl_matrix * VS_min2_VT  )
{
  int m=(*U).size2;
  gsl_vector * UTb=gsl_vector_alloc(m);
  gsl_matrix * Sinv=gsl_matrix_calloc(m,m);
  gsl_vector * y=gsl_vector_alloc(m);
  gsl_blas_dgemv (CblasTrans,1.0,U,b,0.0,UTb);// Generating UTb

  for(int i=0; i<m;i++)gsl_matrix_set(Sinv,i,i,1.0/gsl_matrix_get(S,i,i));

  gsl_blas_dgemv (CblasNoTrans, 1.0,Sinv,UTb,0.0,y);

  // Now c=Vy
  gsl_blas_dgemv (CblasNoTrans, 1.0,V,y,0.0,c);
  // equation Sy=UTb  where y=Vc. S is a square diagonal matrix therefore it is easy to multiply by its inverse

   // At last calculating covariance matrix
   gsl_matrix * S_min2=gsl_matrix_calloc(m,m);
   gsl_matrix * VS_min2 = gsl_matrix_alloc(m,m);
   for(int i=0; i<m;i++)gsl_matrix_set(S_min2,i,i,1.0/pow(gsl_matrix_get(S,i,i),2));
   gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,V,S_min2,0.0,VS_min2);
   gsl_blas_dgemm (CblasNoTrans,CblasTrans, 1.0,VS_min2,V,0.0,VS_min2_VT);

  gsl_vector_free(UTb);
  gsl_matrix_free(Sinv);
  gsl_vector_free(y);
  gsl_matrix_free(S_min2);
  gsl_matrix_free(VS_min2);

}

void ord_lst_sqr_fit(gsl_matrix * A, gsl_vector * b, gsl_vector *xx, gsl_vector * yy, gsl_vector *dyy   , gsl_vector * c,gsl_matrix * VS_min2_VT){
  int n=(*A).size1, m=(*A).size2;
  for(int i=0; i<n;i++){
  double xi=gsl_vector_get(xx,i);
  double yi=gsl_vector_get(yy,i);
  double dyi=gsl_vector_get(dyy,i);
  //double dyi=gsl_vector_get(dy,i);
  gsl_vector_set(b,i,yi/dyi);
   for(int k=0; k<m;k++){gsl_matrix_set(A,i,k,func(k,xi)/dyi);}
  }

  gsl_matrix * S = gsl_matrix_calloc(m,m);
  gsl_matrix * V = gsl_matrix_calloc(m,m);
  gsl_matrix * U = gsl_matrix_alloc(n,m);
  sing_val_de(A,U,S,V);
  sing_val_de_solve(U,S,V,b,c,VS_min2_VT );

 gsl_matrix_free(S);
 gsl_matrix_free(V);
 gsl_matrix_free(U);
}

int main()
{
  // I define my x, y and dy data, which are given in the website under the specific assignment
double  x[]  = {0.100,    1.330,    2.550,    3.780,    5.000,    6.220,    7.450,    8.680,    9.900};
double  y[]  = {-15.300,  0.320,    2.450,    2.750,    2.270,    1.350,    0.157,   -1.230,   -2.750};
double  dy[] = {1.040,    0.594,    0.983,    0.998,    1.110,    0.398,    0.535,    0.968,    0.478};

  int n=sizeof(x)/sizeof(x[0]);
  int m=3;

gsl_vector * xx= gsl_vector_alloc(n);
gsl_vector * yy= gsl_vector_alloc(n);
gsl_vector * dyy= gsl_vector_alloc(n);
gsl_vector * b= gsl_vector_alloc(n);
gsl_matrix * A= gsl_matrix_alloc(n,3);

for (int i = 0; i < n; i++)
{
gsl_vector_set(xx,i,x[i]);
gsl_vector_set(yy,i,y[i]);
gsl_vector_set(dyy,i,dy[i]);
}

for (int i = 0; i < n; i++)
{
printf("%g %g %g \n", x[i],y[i],dy[i]);
}
printf("\n\n");


for (int i = 0; i < n; i++)
{
double xi= gsl_vector_get(xx,i);
double yi= gsl_vector_get(yy,i);
gsl_vector_set(b,i,yi);
for (int k = 0; k < m; k++){gsl_matrix_set(A,i,k,func(k,xi)); }

}

// Implement a function that solves the linear least squares problem Ax=b using your implementation of singular-value decomposition.
gsl_matrix * S = gsl_matrix_calloc(m,m);
gsl_matrix * V = gsl_matrix_calloc(m,m);
gsl_matrix * U = gsl_matrix_alloc(n,m);
gsl_vector * c = gsl_vector_alloc(m);
gsl_matrix * VS_min2_VT = gsl_matrix_alloc(m,m);
sing_val_de(A,U,S,V);

sing_val_de_solve(U,S,V,b,c, VS_min2_VT);
for(double z=0.1;z<=12;z+=0.01)
{// The function
  double s=0, sp=0, sm=0;
  for(int k=0; k<m;k++){
  s+=gsl_vector_get(c,k)*func(k,z);
  sp+=(gsl_vector_get(c,k)+gsl_matrix_get(VS_min2_VT,k,k))*func(k,z);
  sm+=(gsl_vector_get(c,k)-gsl_matrix_get(VS_min2_VT,k,k))*func(k,z);
  }
  printf("%g %g %g %g \n",z,s,sp,sm);
}

//Ordinary least squar--------------------------------------------------------------
  gsl_vector * c2=gsl_vector_alloc(m);
  gsl_matrix * VS_min2_VT2=gsl_matrix_calloc(m,m);
  ord_lst_sqr_fit(A,b,xx,yy,dyy,c2,VS_min2_VT2);
  printf("\n \n");
  for(double z=0.1;z<=12;z+=0.1)
  {// The function
    double s=0, sp=0, sm=0;
    for(int k=0; k<m;k++){
    s+=gsl_vector_get(c2,k)*func(k,z);
    sp+=(gsl_vector_get(c2,k)+gsl_matrix_get(VS_min2_VT2,k,k))*func(k,z);
    sm+=(gsl_vector_get(c2,k)-gsl_matrix_get(VS_min2_VT2,k,k))*func(k,z);
    }
    printf("%g %g %g %g \n",z,s,sp,sm);
  }

  gsl_vector_free(xx);
  gsl_vector_free(yy);
  gsl_vector_free(dyy);
  gsl_vector_free(b);
  gsl_matrix_free(A);



  return 0;
}
