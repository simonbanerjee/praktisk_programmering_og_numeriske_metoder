#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"

void qr_gs_decomp(gsl_matrix* A, gsl_matrix *R){
// In the following because I use vector views, when the columns are changed the Matrix from which the column is read also change. The vector view and the matrix points to the same memory. The means that A-->Q
size_t m=(*A).size2; // n=(*A).size1

for (int i= 0; i < m; i++){ // for loop that sets the diagonal
gsl_vector_view a_i=gsl_matrix_column(A,i);
double R_ii=gsl_blas_dnrm2(&a_i.vector);
gsl_matrix_set(R,i,i,R_ii); // here I set the diagonal
gsl_vector_scale(&a_i.vector,1.0/R_ii); // // This is q_i. The scale function does the scaling and return the result in a_i (a_i=a_i/R_ii)

for(int j=i+1; j<m; j++){
  gsl_vector_view a_j=gsl_matrix_column(A,j);
  double R_ij = 0.0; gsl_blas_ddot(&a_i.vector, &a_j.vector,&R_ij);
  gsl_blas_daxpy (-R_ij,&a_i.vector , &a_j.vector);//This function compute the sum y = a* x + y for the vectors x, y and constand a
  gsl_matrix_set(R,i,j,R_ij);// Filling R
    }
  }
}
