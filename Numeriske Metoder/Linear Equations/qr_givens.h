void qr_givens(gsl_matrix*A);
void qr_givens_QT(gsl_matrix * Q, gsl_vector * b);
void qr_givens_solve(gsl_matrix* Q, gsl_vector* b);
