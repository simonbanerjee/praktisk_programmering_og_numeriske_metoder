#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_givens.h"


void qr_givens(gsl_matrix*A)
{
    int n=(*A).size1, m=(*A).size2;
    for (int q = 0; q < m; q++)
    {
        for (int p =q+1; p < n; p++)
        {
          double theta=atan2(gsl_matrix_get(A,p,q),gsl_matrix_get(A,q,q)); // the angle that zeros the x'_p element

          for(int k = q; k < m; k++)
          {
              double xq=gsl_matrix_get(A,q,k);
              double xp=gsl_matrix_get(A,p,k);
              gsl_matrix_set(A,q,k,xq*cos(theta)+xp*sin(theta));
              gsl_matrix_set(A,p,k,-xq*sin(theta)+xp*cos(theta));
          }
          gsl_matrix_set(A,p,q,theta);
        }
    }
}


void qr_givens_QT(gsl_matrix * Q, gsl_vector * b)
{
  int n=(*Q).size1;
  int m=(*Q).size2;
  for (int q = 0; q < m; q++)
  {
    for (int p = q+1; p < n; p++)
    {
      double theta=gsl_matrix_get(Q,p,q);
      double bq=gsl_vector_get(b,q);
      double bp=gsl_vector_get(b,p);
      gsl_vector_set(b,q,bq*cos(theta)+bp*sin(theta));
      gsl_vector_set(b,p,-bq*sin(theta)+bp*cos(theta));
    }
  }
}


void qr_givens_solve(gsl_matrix* Q, gsl_vector* b)
{
  int m=(*b).size;
  // Doing backsubstitution
  for(int i=m-1; i>=0; i--)
  {
      double ci=0.0;
      for(int j=i+1; j<m; j++)
      {
        ci-=gsl_matrix_get(Q,i,j)*gsl_vector_get(b,j);
      }
      gsl_vector_set(b,i,(gsl_vector_get(b,i)+ci)/gsl_matrix_get(Q,i,i));
  }
}
