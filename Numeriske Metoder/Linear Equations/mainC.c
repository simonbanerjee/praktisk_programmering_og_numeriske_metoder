#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"
#include"qr_givens.h"

// I define a function that prints my matrix
int print_matrix(gsl_matrix *M){
// I create a matrix n x m matrix where n>m
int n=(*M).size1;
int m=(*M).size2;
for (int row = 0; row< n; row++)
{
  for(int columns=0 ; columns<m ; columns++)
    printf("%10.3f", gsl_matrix_get(M,row,columns));
    printf("\n");
}
return 0;
}

// I now create a function that prints a vector

int print_vector(gsl_vector * M){// Function that prints vector
int n=(*M).size;
  for (int row=0; row<n; row++)

         printf("%10.3f     ", gsl_vector_get(M,row));

return 0;
}


  int main(){

  int m=7;
  gsl_matrix *A=gsl_matrix_calloc(m,m);
  gsl_matrix *Q=gsl_matrix_calloc(m,m);
  gsl_vector *b=gsl_vector_calloc(m);

  for(int i=0; i<m; i++){
    for(int j=0; j<m; j++){
      double random_number=rand()/100000000.0;
      gsl_matrix_set(A,i,j,random_number);
    }
  }

  //because I need the matrix A later, I make a copy of A and store as Q

  // Now I print a randomly constructed matrix A
  printf("\n \n The random matrix A \n");
  print_matrix(A);
  gsl_matrix_memcpy(Q,A);
  qr_givens(Q);

printf("QR using the givens rotations\n");
printf("\n \n The upper part is to equal R, where the lower part contanins the angels which zero the corresponding element in the matrix below\n");
print_matrix(Q);

for (int i=0; i<m ; i++){
double rand_v=rand()/10000000.0;
gsl_vector_set(b,i,rand_v); // this generates a random vector
}
printf("A random b vector\n");
print_vector(b);

qr_givens_QT(Q,b);
qr_givens_solve(Q,b);
printf("\n \n x=\n ");
print_vector(b);




printf("\n \n Ax is equal \n");
gsl_vector * bb=gsl_vector_calloc(m);
gsl_blas_dgemv(CblasNoTrans,1.0,A,b,0.0, bb);
print_vector(bb);

gsl_matrix_free(A);
gsl_matrix_free(Q);
gsl_vector_free(b);
gsl_vector_free(bb);


return 0;
}
