#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"

// I define a function that prints my matrix
int print_matrix(gsl_matrix *M){
// I create a matrix n x m matrix where n>m
int n=(*M).size1;
int m=(*M).size2;
for (int row = 0; row< n; row++)
{
  for(int columns=0 ; columns<m ; columns++)
    printf("%10.3f", gsl_matrix_get(M,row,columns));
    printf("\n");
}
return 0;
}

// I now create a function that prints a vector

int print_vector(gsl_vector * M){// Function that prints vector
int n=(*M).size;
  for (int row=0; row<n; row++)

         printf("%10.3f     ", gsl_vector_get(M,row));

return 0;
}

int main(){

int m=7;
gsl_matrix *A=gsl_matrix_calloc(m,m);
gsl_matrix *Q=gsl_matrix_calloc(m,m);
gsl_matrix *B=gsl_matrix_calloc(m,m);

for(int i=0; i<m; i++){
  for(int j=0; j<m; j++){
    double random_number=rand()/100000000.0;
    gsl_matrix_set(A,i,j,random_number);
  }
}

//because I need the matrix A later, I make a copy of A and store as Q
gsl_matrix_memcpy(Q,A);

// Now I print a randomly constructed matrix A
printf("\n \n The random matrix A \n");
print_matrix(A);

qr_gs_inverse(Q,B); // B shoudl be A^(⁻1). When A is returned it is Q.
printf("\n \n The inverse of B is \n");
print_matrix(B);

printf("\n \n AB should give the indentity matrix\n");
gsl_matrix *AA_inv=gsl_matrix_calloc(m,m);
gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,A,B,0.0,AA_inv);
print_matrix(AA_inv);

  return 0;
}
