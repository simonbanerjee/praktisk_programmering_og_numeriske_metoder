#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"

void qr_gs_inverse(gsl_matrix* A, gsl_matrix* B){
// The inverse of a matirx can be found solving a system of linear equation on the form Ax_i=e_i

int m=(*A).size1;
gsl_vector * x=gsl_vector_calloc(m);
gsl_vector * b=gsl_vector_calloc(m);
gsl_matrix * R=gsl_matrix_calloc(m,m);

qr_gs_decomp(A,R);
for(int i=0;i<m;i++)
{
  gsl_vector_set(b,i,1.0);// Generates the basis vector
  qr_gs_solve(A,R,b,x);// the solution for the x_i'th
  gsl_vector_set(b,i,0.0);
  gsl_matrix_set_col(B,i,x);
}








}
