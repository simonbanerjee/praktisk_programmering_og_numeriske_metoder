#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"
#include<time.h>

// I define a function that prints my matrix
int print_matrix(gsl_matrix *M){
// I create a matrix n x m matrix where n>m
int n=(*M).size1;
int m=(*M).size2;
for (int row = 0; row< n; row++)
{
  for(int columns=0 ; columns<m ; columns++)
    printf("%10.3f", gsl_matrix_get(M,row,columns));
    printf("\n");
}
return 0;
}

// I now create a function that prints a vector

int print_vector(gsl_vector * M){// Function that prints vector
int n=(*M).size;
  for (int row=0; row<n; row++)

         printf("%10.3f     ", gsl_vector_get(M,row));

return 0;
}


int main(){
srand((unsigned)time(NULL));

size_t n=10, m=6; //size of my matrix is a 10 x 6 matrix (n x m, where n>m)

// I allocate memory for the the A and R matrix
gsl_matrix *A=gsl_matrix_alloc(n,m);
gsl_matrix *R=gsl_matrix_calloc(m,m); // R has zeros on every entry

for(int i=0; i<n; i++){
  for(int j=0; j<m; j++){
    double x=rand()/1000000.0;
    gsl_matrix_set(A,i,j,x);
  }
}

// Part A1----------------------------------------------------------------------
// Testing qr_gs_decomp --------------------------------------------------------
printf("\n\n Part A1\n" );
printf("The generated matrix \n");
print_matrix(A);
qr_gs_decomp(A,R);

printf("\n \n Matrix R \n");
print_matrix(R);

gsl_matrix *QTQ=gsl_matrix_calloc(m,m);
// Function that computes the matrix-matrix product and sum C = \alpha op(A) op(B) + \beta C where op(A) = A, A^T, A^H. Result returned in zero_matrix
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,QTQ);

printf("\n \n A^TA should give the identity matrix\n");
print_matrix(QTQ);

printf("\n \n QR=A \n");
gsl_matrix *QR=gsl_matrix_calloc(n,m);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,QR);
print_matrix(QR);

// Part A2----------------------------------------------------------------------
//Testing qr_gs_solve ----------------------------------------------------------

printf("\n\nPart A2\n");
printf("\n \n Printing a random matrix A \n");
gsl_matrix * AA=gsl_matrix_calloc(m,m);
gsl_matrix * RR=gsl_matrix_calloc(m,m);
gsl_matrix * QQ=gsl_matrix_calloc(m,m);

for(int i=0; i<m; i++){
  for(int j=0; j<m ; j++){
  double random_number=rand()/1000000.0;
  gsl_matrix_set(AA,i,j,random_number);
  }
}
print_matrix(AA);


// Because I need AA later, I make a copy of AA and store is  a matrix called QQ
gsl_matrix_memcpy(QQ,AA);
printf("\n \n RR\n");
qr_gs_decomp(QQ,RR);
print_matrix(RR);


printf("\n \n QR=A\n");
  gsl_matrix * QQRR=gsl_matrix_calloc (m,m);
  gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,QQ,RR,0.0,QQRR);
  print_matrix(QQRR);



// creating a vector b with the same size
gsl_vector * b=gsl_vector_calloc(m);
gsl_vector * x=gsl_vector_calloc(m);
for (int i=0; i<m ; i++){
double rand_v=rand()/1000000.0;
gsl_vector_set(b,i,rand_v); // this generates a random vector
}
printf("\n b^T\n");
print_vector(b);

qr_gs_solve(QQ,RR,b,x);
gsl_vector * bb=gsl_vector_calloc(m);

gsl_blas_dgemv(CblasNoTrans,1.0,AA,x,0.0, bb);
printf("\n x^T \n");
print_vector(x);

printf("\n \n Ax \n");
print_vector(bb);
printf("\n");


gsl_matrix_free(A);
gsl_matrix_free(R);
gsl_matrix_free(QTQ);
gsl_matrix_free(QR);
gsl_vector_free(b);
gsl_vector_free(x);
gsl_matrix_free(AA);
gsl_matrix_free(QQ);
gsl_matrix_free(RR);
gsl_matrix_free(QQRR);


  return 0;
}
