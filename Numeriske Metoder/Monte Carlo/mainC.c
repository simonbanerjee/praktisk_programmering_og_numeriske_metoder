#include<stdlib.h>
#include<math.h>
#include<stdio.h>
void montecarlo_plain(double f(double*),double *a, double *b,double * result, double *error, int dim, int N);
void randomx(double *x, double * a, double *b, int dim);

double adpt_one_D_integ(double f(double*), double a, double b, double eps, double acc,double *error,int N,int dim, int nrec);

double adapt24(double f(double),double a, double b, double eps, double acc, double f2, double f3,int nrec);

double adapt(double f(double),double a, double b, double eps, double acc);

double f_pi(double x[]){
	return 4.0*sqrt(1.0-pow(1.0-x[0],2));
}
double f_pi_ordenary(double x){
	return 4.0*sqrt(1.0-pow(1.0-x,2));
}


int main(){

// Test integration
	printf("Test integration:\n\n ");
	double result, error, eps=1e-3, acc=1e-3;
	int dim=1, N=1000000;
	double a=0, b=1;
	result=adpt_one_D_integ(f_pi,a,b,eps,acc,&error,N,dim,0);

	printf("Integral of f(x)=4*sqrt(1-(1-x)^2)  from 0 to 1");
	printf("\nIntegration \t %1.4f",result);
	printf("\nExact answer \t %1.4f",M_PI);
	printf("\nError estimate \t %1.4f", error);

// Compare with my ordinary adaptive integrator.
	printf("\n \n Ordinary adaptive integrator");
	double Ordenary=adapt(f_pi_ordenary,a,b,eps,acc);
	printf("\nIntegration \t %1.4f",Ordenary);
	printf("\nExact answer \t %1.4f",M_PI);
	printf("\nError estimate \t %1.4f\n", acc+eps*fabs(Ordenary));


}
