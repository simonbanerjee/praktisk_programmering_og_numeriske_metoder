#include<stdlib.h>
#include<math.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
void montecarlo_plain(double f(double*),double *a, double *b,double * result, double *error, int dim, int N);
void randomx(double *x, double * a, double *b, int dim);


double f_pi(double x[]){
	return 4.0*sqrt(1.0-pow(1.0-x[0],2));
}

double f_gauss2(double x[]){
	return exp(-x[0]*x[0])*exp(-x[1]*x[1]);
}

double f(double x[]){
	return 1.0/(1.0-cos(x[0])*cos(x[1])*cos(x[2]))*1.0/(pow(M_PI,3));

}

int main()
{

  	double result=0, error=0;
  	int dim=1, Max_iter=1000;
  	gsl_vector * Nx=gsl_vector_alloc(Max_iter);
  	gsl_vector * E=gsl_vector_alloc(Max_iter);
  	double a[]={0}, b[]={1};
  	for(int N=0;N<Max_iter;N++){
  		montecarlo_plain(f_pi,a,b,&result,&error,dim,N);
  		printf("%i %g\n",N,error);
  		gsl_vector_set(Nx,N,N);
  		gsl_vector_set(E,N,error);

  	}
  	printf("\n\n");
  	for(int i=1;i<Max_iter;i++){printf("%i %g\n",i,1.0/(sqrt(i)));}




return 0;
}
