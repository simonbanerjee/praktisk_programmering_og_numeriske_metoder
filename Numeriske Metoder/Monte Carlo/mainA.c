#include<stdlib.h>
#include<math.h>
#include<stdio.h>
void montecarlo_plain(double f(double*),double *a, double *b,double * result, double *error, int dim, int N);
void randomx(double *x, double * a, double *b, int dim);


// I chose to calculate these two integrals
double f_pi(double x[]){
	return 4.0*sqrt(1.0-pow(1.0-x[0],2));
}

double f_exp2(double x[]){
	return exp(-x[0]*x[0])*exp(-x[1]*x[1]);
}

//integral given in the assignment
double f(double x[]){
	return 1.0/(1.0-cos(x[0])*cos(x[1])*cos(x[2]))*1.0/pow(M_PI,3);

}

int main(){
  printf("Testing integration on two test functions\n");
  double result=0, error=0;
  int dim=1, N=1000000;

  //first text function
  double a[]={0}, b[]={1};
  montecarlo_plain(f_pi,a,b,&result,&error,dim,N);


  printf("Integral of f(x)=4*sqrt(1-(1-x)^2)  from 0 to 1");
  printf("\nIntegration \t %1.4f",result);
  printf("\nExact answer \t %1.4f",M_PI);
  printf("\nError estimate \t %1.4f", error);

  //next test function

  double a2[]={-5,-5}, b2[]={5,5};
  dim=2;
  montecarlo_plain(f_exp2,a2,b2,&result,&error,dim,N);

  printf("\n\nIntegral of f(x)=exp(-x²)*exp(-y²)  from -10 to 10 for both x and y");
  printf("\nIntegration \t %1.4f",result);
  printf("\nExact answer (infinite intervals)\t %1.4f",M_PI);
  printf("\nError estimate \t %1.4f", error);

  // assignment function
  double a3[]={0,0,0}, b3[]={M_PI,M_PI,M_PI};
  dim=3; N=10000;
  montecarlo_plain(f,a3,b3,&result,&error,dim,N);
  double exact=1.3932039296856768591842462603255;
  printf("\n\nIntegral of f(x)=1.0/(1.0-cos(x[0])*cos(x[1])*cos(x[2]))*1.0/(pow(M_PI,3)) from 0 to pi for both x, y and z");
  printf("\nIntegration \t %1.10f",result);
  printf("\nExact answer \t %1.10f",exact);
  printf("\nError estimate \t %1.10f\n", error);













  return 0;
}
