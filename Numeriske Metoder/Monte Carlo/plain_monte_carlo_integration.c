#include<stdlib.h>
#include<math.h>
#include<stdio.h>
#include <assert.h>
#define RND ((double)rand()/RAND_MAX)

void randomx(double *x, double a[], double b[] , int dim)
{
  for(int i=0; i<dim; i++)
  { x[i]= a[i]+RND*(b[i]-a[i]); }
}

void montecarlo_plain(double f(double*),double a[], double b[], double * result, double *error, int dim, int N)
{
  double V=1, sum=0, sum2=0, x[dim];
  for (int i = 0; i < dim; i++)
  {
    V*=b[i]-a[i]; // The volume of integration space
  }

  for (int i = 0; i < N; i++)
  {
    randomx(x,a,b,dim);
    sum+=f(x);
    sum2+=f(x)*f(x);
  }

double mean = sum/N;
*result = V*mean;
double var=(sum2/N-mean*mean);
*error = V*sqrt(var/N);
}

double adpt_one_D_integ(double f(double*), double a, double b, double eps, double acc,double* error, int N,int dim, int nrec){
	assert(nrec<1000000);
	double a_vec[]={a}, b_vec[]={b};// Montecarlo function takes vectors
	double result=0;
	montecarlo_plain(f,a_vec,b_vec,&result,error,dim,N);

	double error1, error2;
	if(*error*(nrec+1.0)<=acc){return result;}
	else{ // subdivision if error >tol
		double result1=adpt_one_D_integ(f,a,(b+a)/2,eps,acc/sqrt(2.0),&error1,N,dim, nrec+1);
		double result2=adpt_one_D_integ(f,(b+a)/2,b,eps,acc/sqrt(2.0),&error2, N, dim, nrec+1);
	*error=error1+error2;
	return result1+result2;
	}
}



// Compare with your ordinary adaptive integrator.

double adapt24(double f(double),double a, double b, double eps, double acc, double f2, double f3,int nrec){
	assert(nrec<1000000);
	double f1=f(a+(b-a)*1.0/6.0), f4=f(a+(b-a)*5.0/6.0); //eq 51 from lecture notes
	double Q=(2.0/6.0*f1+1.0/6.0*f2+1.0/6.0*f3+2.0/6.0*f4)*(b-a);// eq 44
	double q=(f1+f2+f3+f4)/4.0*(b-a);// eq 45
	double dQ=fabs(Q-q);// eq 46
	double tol=acc+eps*fabs(Q); //Right hand side of eq. 47
	if(dQ<tol){return Q;}

	else{ // subdivision if error >tol
		double Q1=adapt24(f,a,(b+a)/2,eps,acc/sqrt(2.0), f1,f2, nrec+1);
		double Q2=adapt24(f,(b+a)/2,b,eps,acc/sqrt(2.0), f3,f4, nrec+1);
	return Q1+Q2;
	}
}

double adapt(double f(double),double a, double b, double eps, double acc){
	int nrec=0;
	double f2=f(a+(b-a)*2.0/6.0), f3=f(a+(b-a)*4.0/6.0);
	return adapt24(f,a,b,eps, acc,f2,f3,nrec);

}
