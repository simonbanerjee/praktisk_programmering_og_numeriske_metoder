#include<math.h>
#include<stdlib.h>
#include<assert.h>
#include"functions.h"
#include<stdio.h>

double adapt24(double f(double),double a, double b, double eps, double acc, double f2, double f3,int nrec){
	assert(nrec<1000000);
	double f1=f(a+(b-a)*1.0/6.0), f4=f(a+(b-a)*5.0/6.0); //eq 51 from lecture notes
	double Q=(2.0/6.0*f1+1.0/6.0*f2+1.0/6.0*f3+2.0/6.0*f4)*(b-a);// eq 44
	double q=(f1+f2+f3+f4)/4.0*(b-a);// eq 45
	double dQ=fabs(Q-q);// eq 46
	double tol=acc+eps*fabs(Q); //Right hand side of eq. 47
	if(dQ<tol){return Q;}

	else{ // subdivision if error >tol
		double Q1=adapt24(f,a,(b+a)/2,eps,acc/sqrt(2.0), f1,f2, nrec+1);
		double Q2=adapt24(f,(b+a)/2,b,eps,acc/sqrt(2.0), f3,f4, nrec+1);
	return Q1+Q2;
	}
}

double adapt(double f(double),double a, double b, double eps, double acc){
	int nrec=0;
	double f2=f(a+(b-a)*2.0/6.0), f3=f(a+(b-a)*4.0/6.0);
	return adapt24(f,a,b,eps, acc,f2,f3,nrec);

}
double adapt_general(double f(double),double a, double b, double f2, double f3, double eps, double acc){
	int nrec=0;
	return adapt24(f,a,b,eps, acc,f2,f3,nrec);

}



double integrate_general(double f(double), double a, double b, double eps, double acc){
	double A=0.0, B=1.0;
	double g1(double t){
		return (f((1.0-t)/(t))+f(-(1.0-t)/t))/t/t;  // Eq 58 -inf to inf
	}

	double g2(double t){

		return f(a+t/(1.0-t))/pow(1.0-t,2); // eq 59 a to inf
	}
	double g3(double t){

		return f(b-(1.0-t)/t)/t/t; // eq 62 -inf to b
	}

	 double f2, f3;
	if(isinf(-a) && isinf(b)){

		f2=g1(A+(B-A)*2.0/6.0);
		f3=g1(A+(B-A)*4.0/6.0);
		return adapt_general(g1,A,B,f2,f3,eps,acc);
	}
	else if(isinf(b)){
		f2=g2(A+(B-A)*2.0/6.0);
		f3=g2(A+(B-A)*4.0/6.0);
		return adapt_general(g2,A,B,f2,f3,eps,acc);
	}
	else if(isinf(-a)){
		f2=g3(A+(B-A)*2.0/6.0);
		f3=g3(A+(B-A)*4.0/6.0);
		return adapt_general(g3,A,B,f2,f3,eps,acc);
	}
	else{
		f2=f(a+(b-a)*2.0/6.0);
		f3=f(a+(b-a)*4.0/6.0);
		return adapt_general(f,a,b,f2,f3,eps,acc);
	}


}
