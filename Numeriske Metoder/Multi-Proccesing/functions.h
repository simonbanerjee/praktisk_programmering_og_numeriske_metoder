double adapt24(double f(double),double a, double b, double eps, double acc, double f2, double f3,int nrec);
double adapt(double f(double),double a, double b, double eps, double acc);
double adapt_general(double f(double),double a, double b, double f2, double f3, double eps, double acc);
double integrate_general(double f(double), double a, double b, double eps, double acc);
