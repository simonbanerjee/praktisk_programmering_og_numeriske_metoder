#include<math.h>
#include<stdlib.h>
#include<assert.h>
#include"functions.h"
#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<omp.h>

int main()
{
printf("Multi-Processing of Adaptive Integration Assignment A and B \n");
#pragma omp parallel sections
{
#pragma omp section
{
	printf("Thread 1, Assignment 1\n");
	int calls=0;

	double f1(double x){
		calls++;
		return sqrt(x);
	}
	double f1_gsl(double x,void* params){
		calls++;
		return sqrt(x);
	}

	double f2(double x){
		calls++;
		return 1.0/sqrt(x);
	}
	double f2_gsl(double x,void *params){
		calls++;
		return 1.0/sqrt(x);
	}

	double f3(double x){
		calls++;
		return log(x)/sqrt(x);
	}
	double f3_gsl(double x, void *params){
		calls++;
		return log(x)/sqrt(x);
	}
	double f_pi(double x){
		calls++;
		return 4.0*sqrt(1.0-pow(1.0-x,2));
	}
	double f_pi_gsl(double x, void* params){
		calls++;
		return 4.0*sqrt(1.0-pow(1.0-x,2));
	}


	// Part A
	double acc=1e-4, eps=1e-4;
	double a=0.0, b=1.0;


	 double Q1=adapt(f1,a,b,eps,acc);
	 printf("1:Integral of f(x)=sqrt(x) from 0 to 1");
	 printf("\n1:Integration \t %1.4f",Q1);
	 printf("\n1:Exact answer \t %1.4f",2.0/3.0);
	 printf("\n1:Error estimate \t %1.4f", acc+eps*fabs(Q1));
	 printf("\n1:Number of calls \t %i\n",calls );

	 calls=0;
	 double Q2=adapt(f2,a,b,eps,acc);
	 printf("\n \n1:Integral of f(x)=1/sqrt(x) from 0 to 1");
	 printf("\n1:Integration \t %1.4f",Q2);
	 printf("\n1:Error estimate \t %1.4f", acc+eps*fabs(Q2));
	 printf("\n1:Exact answer \t %1.4f",2.0);
	 printf("\n1:Number of calls \t %i\n ",calls );

	 calls=0;
	 double Q3=adapt(f3,a,b,eps,acc);
	 printf("\n \n1:Integral of f(x)=ln(x)/sqrt(x) from 0 to 1");
	 printf("\n1:Integration \t %1.4f",Q3);
	 printf("\n1:Exact answer \t %1.4f",-4.0);
	 printf("\n1:Error estimate \t %1.4f", acc+eps*fabs(Q3));
	 printf("\n1:Number of calls \t %i \n",calls );

	 acc=1e-14, eps=1e-14;
	 calls=0;
	 double Q4=adapt(f_pi,a,b,eps,acc);
	 printf("\n \n1:Integral of f(x)=4*sqrt(1-(1-x)^2) from 0 to 1");
	 printf("\n1:Integration \t %1.16g",Q4);
	 printf("\n1:Exact answer \t %1.16g",M_PI);
	 printf("\n1:Error estimate \t %1.16g", acc+eps*fabs(Q4));
	 printf("\n1:Number of calls \t %i",calls);
	 printf("\n1:Because I use double's it is only possilbe to write out the 16 first digits \n");

}

#pragma omp section
{
	// Part B
	int calls;
	double f_pi(double x){
		calls++;
		return 4.0*sqrt(1.0-pow(1.0-x,2));
	}
	double f_pi_gsl(double x, void* params){
		calls++;
		return 4.0*sqrt(1.0-pow(1.0-x,2));
	}

	double f_exp(double x){
		calls++;
		return exp(-x*x);
	}
	double f_exp_gsl(double x,void * params){
		calls++;
		return exp(-x*x);
	}

	double f_1_div_xx(double x){
		calls++;
		return 1.0/(x*x);
	}

	double f_1_div_xx_gsl(double x,void * params){
		calls++;
		return 1.0/(x*x);
	}

	double f_xexp(double x){
		calls++;
		return x*exp(-x*x);
	}
	double f_xexp_gsl(double x,void * params){
		calls++;
		return x*exp(-x*x);
	}

	printf("\n Thread 2, Assignment 2: infinite integrals\n");
	double acc, eps, a, b;


  size_t limit = 100; // Number of iterations
  double result,err; // Setting integrations limits, absolut error and relative error.
  gsl_integration_workspace * workspace =gsl_integration_workspace_alloc(limit);// Allocating workspace
  acc=1e-4, eps=1e-4;

  printf("\n \n\n \n \n Part B: Generalization of integration");
  a=0.0, b=1.0; calls=0;
  double int_pi=integrate_general(f_pi,a,b,eps,acc);
  gsl_function f; //
  f.function =f_pi_gsl;
  f.params =NULL;
  gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result,&err);
  printf("\n\n2:First I test that my generalized works for finite intervals by doing the integration that should give pi");
  printf("\n2:Integration \t %1.4f",int_pi);
  printf("\n2:Exact answer \t %1.4f",M_PI);
  printf("\n2:Error estimate \t %1.4f", acc+eps*fabs(int_pi));
  printf("\n2:Number of calls \t %i",calls);
  printf("\n2:gsl answer \t %1.4g\n",result);

  printf("\n\n2:Now trying for some more complex integrals");calls=0;
  double int_exp=integrate_general(f_exp,-INFINITY,INFINITY,eps,acc);
  f.function =f_exp_gsl;
  gsl_integration_qagi(&f,acc,eps,limit,workspace,&result,&err);
	printf("\n\n2:First I try int exp(-x^2) from -inf to inf. The integrals gives %g, and should give sqrt(pi)=%g",int_exp,sqrt(M_PI));
	printf("\n2:Integration \t %1.4f",int_exp);
  printf("\n2:Exact answer \t %1.4f",sqrt(M_PI));
  printf("\n2:Number of calls \t %i",calls);
	printf("\n2:Error estimate \t %1.4f", acc+eps*fabs(int_exp));
  printf("\n2:gsl answer \t %1.4g\n",result);

  calls=0;
  double int_1_xx=integrate_general(f_1_div_xx,1,INFINITY,eps,acc);
  f.function =f_1_div_xx_gsl;
  gsl_integration_qagiu(&f,1.0,acc,eps,limit,workspace,&result,&err);
	printf("\n\n2:Next I try int 1/x^2 from 1 to inf");
	printf("\n2:Integration \t %1.4f", int_1_xx);
  printf("\n2:Exact answer \t %1.4f",1.0);
  printf("\n2:Error estimate \t %1.4f", acc+eps*fabs(int_1_xx));
  printf("\n2:Number of calls \t %i",calls);
  printf("\n2:gsl answer \t %1.4g\n",result);

  calls=0;
  double int_xexp=integrate_general(f_xexp,INFINITY,1,eps,acc);
  f.function =f_xexp_gsl;
  gsl_integration_qagil(&f,1.0,acc,eps,limit,workspace,&result,&err);
  printf("\n\n2:Now int x*exp(-x^2) from -inf to 1");
  printf("\n2:Integration \t %1.4f", int_xexp);
  printf("\n2:Exact answer \t %1.4f",-1.0/(2*M_E));
  printf("\n2:Error estimate \t %1.4f", acc+eps*fabs(int_xexp));
  printf("\n2:Number of calls \t %i",calls);
  printf("\n2:gsl answer \t %1.4g \n",result);

}
}
return 0;
}
