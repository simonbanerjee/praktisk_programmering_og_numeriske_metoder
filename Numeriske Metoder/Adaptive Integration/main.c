#include<math.h>
#include<stdlib.h>
#include<assert.h>
#include"functions.h"
#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>


int main(){
int calls=0;

double f1(double x){
	calls++;
	return sqrt(x);
}
double f1_gsl(double x,void* params){
	calls++;
	return sqrt(x);
}

double f2(double x){
	calls++;
	return 1.0/sqrt(x);
}
double f2_gsl(double x,void *params){
	calls++;
	return 1.0/sqrt(x);
}

double f3(double x){
	calls++;
	return log(x)/sqrt(x);
}
double f3_gsl(double x, void *params){
	calls++;
	return log(x)/sqrt(x);
}

double f_pi(double x){
	calls++;
	return 4.0*sqrt(1.0-pow(1.0-x,2));
}
double f_pi_gsl(double x, void* params){
	calls++;
	return 4.0*sqrt(1.0-pow(1.0-x,2));
}

double f_exp(double x){
	calls++;
	return exp(-x*x);
}
double f_exp_gsl(double x,void * params){
	calls++;
	return exp(-x*x);
}

double f_1_div_xx(double x){
	calls++;
	return 1.0/(x*x);
}

double f_1_div_xx_gsl(double x,void * params){
	calls++;
	return 1.0/(x*x);
}

double f_xexp(double x){
	calls++;
	return x*exp(-x*x);
}
double f_xexp_gsl(double x,void * params){
	calls++;
	return x*exp(-x*x);
}


// Part A
double acc=1e-4, eps=1e-4;
double a=0.0, b=1.0;


 double Q1=adapt(f1,a,b,eps,acc);
 printf("Integral of f(x)=sqrt(x) from 0 to 1");
 printf("\nIntegration \t %1.4f",Q1);
 printf("\nExact answer \t %1.4f",2.0/3.0);
 printf("\nError estimate \t %1.4f", acc+eps*fabs(Q1));
 printf("\nNumber of calls \t %i\n",calls );

 calls=0;
 double Q2=adapt(f2,a,b,eps,acc);
 printf("\n \nIntegral of f(x)=1/sqrt(x) from 0 to 1");
 printf("\nIntegration \t %1.4f",Q2);
 printf("\nExact answer \t %1.4f",2.0);
 printf("\nError estimate \t %1.4f", acc+eps*fabs(Q2));
 printf("\nNumber of calls \t %i\n ",calls );

 calls=0;
 double Q3=adapt(f3,a,b,eps,acc);
 printf("\n \nIntegral of f(x)=ln(x)/sqrt(x) from 0 to 1");
 printf("\nIntegration \t %1.4f",Q3);
 printf("\nExact answer \t %1.4f",-4.0);
 printf("\nError estimate \t %1.4f", acc+eps*fabs(Q3));
 printf("\nNumber of calls \t %i \n",calls );

 acc=1e-14, eps=1e-14;
 calls=0;
 double Q4=adapt(f_pi,a,b,eps,acc);
 printf("\n \nIntegral of f(x)=4*sqrt(1-(1-x)^2) from 0 to 1");
 printf("\nIntegration \t %1.16g",Q4);
 printf("\nExact answer \t %1.16g",M_PI);
 printf("\nError estimate \t %1.16g", acc+eps*fabs(Q4));
 printf("\nNumber of calls \t %i",calls);
 printf("\nBecause I use double's it is only possilbe to write out the 16 first digits \n");

 // Part B
 size_t limit = 100; // Number of iterations
 double result,err; // Setting integrations limits, absolut error and relative error.
 gsl_integration_workspace * workspace =gsl_integration_workspace_alloc(limit);// Allocating workspace
 acc=1e-4, eps=1e-4;

 printf("\n \n\n \n \nPart B: Generalization of integration");
 a=0.0, b=1.0; calls=0;
 double int_pi=integrate_general(f_pi,a,b,eps,acc);
 gsl_function f; //
 f.function =f_pi_gsl;
 f.params =NULL;
 gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result,&err);
 printf("\nFirst i test that my generalized works for finite intervals by doing the integration that should give pi");
 printf("\nIntegration \t %1.4f",int_pi);
 printf("\nExact answer \t %1.4f",M_PI);
 printf("\nError estimate \t %1.4f", acc+eps*fabs(int_pi));
 printf("\nNumber of calls \t %i",calls);
 printf("\ngsl answer \t %1.4g\n",result);

 printf("\n\nNow trying for some more complex integrals");calls=0;
 double int_exp=integrate_general(f_exp,-INFINITY,INFINITY,eps,acc);
 f.function =f_exp_gsl;
 gsl_integration_qagi(&f,acc,eps,limit,workspace,&result,&err);
 printf("\nFirst I try int exp(-x^2) from -inf to inf. The integrals gives %g, and should give sqrt(pi)=%g",int_exp,sqrt(M_PI));
 printf("\nIntegration \t %1.4f",int_exp);
 printf("\nExact answer \t %1.4f",sqrt(M_PI));
 printf("\nError estimate \t %1.4f", acc+eps*fabs(int_exp));
 printf("\nNumber of calls \t %i",calls);
 printf("\ngsl answer \t %1.4g\n",result);

 calls=0;
 double int_1_xx=integrate_general(f_1_div_xx,1,INFINITY,eps,acc);
 f.function =f_1_div_xx_gsl;
 gsl_integration_qagiu(&f,1.0,acc,eps,limit,workspace,&result,&err);
 printf("\n\nNext I try int 1/x^2 from 1 to inf");
 printf("\nIntegration \t %1.4f", int_1_xx);
 printf("\nExact answer \t %1.4f",1.0);
 printf("\nError estimate \t %1.4f", acc+eps*fabs(int_1_xx));
 printf("\nNumber of calls \t %i",calls);
 printf("\ngsl answer \t %1.4g\n",result);

 calls=0;
 double int_xexp=integrate_general(f_xexp,INFINITY,1,eps,acc);
 f.function =f_xexp_gsl;
 gsl_integration_qagil(&f,1.0,acc,eps,limit,workspace,&result,&err);
 printf("\n\nNow int x*exp(-x^2) from -inf to 1");
 printf("\nIntegration \t %1.4f", int_xexp);
 printf("\nExact answer \t %1.4f",-1.0/(2*M_E));
 printf("\nError estimate \t %1.4f", acc+eps*fabs(int_xexp));
 printf("\nNumber of calls \t %i",calls);
 printf("\ngsl answer \t %1.4g \n",result);




return 0;
}
