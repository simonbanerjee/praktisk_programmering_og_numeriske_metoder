#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"functions.h"


int newton(double f(gsl_vector* x),void gradient(gsl_vector * x, gsl_vector * df), void hessian(gsl_vector * x, gsl_matrix * H), gsl_vector * x,double eps){

double a=10e-2;
int n=(*x).size;

gsl_vector * df =gsl_vector_alloc(n);
gsl_vector * z =gsl_vector_alloc(n);
gsl_vector * Dx =gsl_vector_alloc(n);

gsl_matrix * R =gsl_matrix_alloc(n,n);
gsl_matrix * H =gsl_matrix_alloc(n,n);




int calls = 0;
double DxTdf= 0 ;

do{ hessian(x,H) ; gradient(x,df); calls++;
// The form of the equation is Dx=-f(x) where I solve for Dx
qr_gs_decomp(H,R);
gsl_vector_scale(df,-1.0);
qr_gs_solve(H,R,df,Dx);
double lambda = 1.0;
do{
  lambda/=2;
  gsl_vector_memcpy(z,x);
  gsl_blas_daxpy(lambda,Dx,z);
  gsl_blas_ddot(Dx,df, &DxTdf);
}while(abs(f(z)) > abs(f(x)) +a*lambda*DxTdf);

gsl_vector_memcpy(x,z);
if(calls > 1000000) break;
} while(gsl_blas_dnrm2(df)>eps);



return calls;
}

void gradient(double f(gsl_vector* x), gsl_vector* x, gsl_vector* df, double dx){
  double fx = f(x);
  int n = (*x).size;
  for (int i = 0; i < n; i++){
    gsl_vector_set(x,i,gsl_vector_get(x,i)+dx);
    gsl_vector_set(df,i,(f(x)-fx)/dx);
    gsl_vector_set(x,i,gsl_vector_get(x,i)-dx);
  }
}

int Quasi_Newton(double f(gsl_vector* x),gsl_vector * x,double eps,double dx){
	int n=(*x).size;
	gsl_matrix * H=gsl_matrix_alloc(n,n);// H inverse
	gsl_matrix_set_identity (H);
	gsl_matrix * dH=gsl_matrix_alloc(n,n);// Correction term
	gsl_vector * dfx=gsl_vector_alloc(n);
	gsl_vector * dfz=gsl_vector_alloc(n);
	gsl_vector * s=gsl_vector_alloc(n);
	gsl_vector * z=gsl_vector_alloc(n);
	gsl_vector * y=gsl_vector_alloc(n);
	gsl_vector * Dx=gsl_vector_alloc(n);
	gsl_vector * H_times_y=gsl_vector_alloc(n);
	gsl_vector * sTH=gsl_vector_alloc(n);
	gsl_matrix * Numerator=gsl_matrix_alloc(n,n);
	gsl_vector * Hs=gsl_vector_alloc(n);
	gsl_vector * u=gsl_vector_alloc(n);

	gradient(f,x,dfx,dx);
	double fx=f(x);

	int calls=0; double sdf, de, fz, alpha=0.1;
	do{ calls++;
		gsl_blas_dgemv (CblasNoTrans,1.0,H,dfx,0,Dx);
		gsl_vector_scale(Dx,-1);

		gsl_vector_memcpy(s,Dx);// setting s
		gsl_vector_scale(s,2);

	do{
		gsl_vector_scale(s,0.5);
		gsl_vector_memcpy(z,x);
		gsl_vector_add(z,s);
		fz=f(z);
		gsl_blas_ddot(s,dfx,&sdf);//Delta(x)*grad(f(x)) eq. 8
		if(fabs(fz) < fabs(fx) + alpha*sdf){break;}
		if(gsl_blas_dnrm2(s) < dx) {gsl_matrix_set_identity(H); break;}
	}while(fabs(fz) > fabs(fx) + alpha*sdf);
	gradient(f,z,dfz,dx);

	gsl_vector_memcpy(y,dfz);
	gsl_vector_sub (y,dfx);// Grad(f(x))-grad(f(x+s))
	gsl_blas_dgemv(CblasNoTrans,1.0,H,y,0.0,H_times_y);

	// correction to H
	gsl_vector_memcpy(u,s);
	gsl_vector_sub(u,H_times_y);
	for (int i=0; i<n; i++) for (int j=0; j<n; j++)gsl_matrix_set(Numerator,i,j,gsl_vector_get(u,i)*gsl_vector_get(s,j));

	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,Numerator,H,0.0,dH);// Numerator eq 12
	gsl_blas_dgemv(CblasNoTrans,1.0,H,s,0.0,Hs);
	gsl_blas_ddot (y,Hs,&de); // Determinator

	gsl_matrix_scale (dH,1/de);
	gsl_matrix_add (H,dH);

	gsl_vector_memcpy(x,z);
	gsl_vector_memcpy(dfx,dfz);
	fx=fz;
  }while(gsl_blas_dnrm2(dfx)>eps);



  gsl_matrix_free(H);
  gsl_matrix_free(dH);
  gsl_vector_free(s);
  gsl_vector_free(dfx);
  gsl_vector_free(dfz);
  gsl_vector_free(y);
  gsl_vector_free(z);
  gsl_vector_free(Dx);
  gsl_vector_free(sTH);
  gsl_vector_free(H_times_y);
  gsl_matrix_free(Numerator);
return calls;
}

int newtonRoot(void f(gsl_vector * x, gsl_vector * fx),gsl_vector * x,double dx,double eps){
  int n=(*x).size;
  gsl_vector * fx=gsl_vector_alloc(n);
  gsl_vector * df=gsl_vector_alloc(n);
  gsl_vector * xx=gsl_vector_alloc(n);
  gsl_vector * fxx=gsl_vector_alloc(n);
  gsl_matrix * J=gsl_matrix_alloc(n,n);
  gsl_matrix * R=gsl_matrix_alloc(n,n);
  gsl_vector * Dx=gsl_vector_alloc(n);// vector which will contain coefficients

  int calls=0;

	do{f(x,fx); calls++;
 		for(int j=0;j<n;j++){// Numerical Jacobian matrix
		gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
		f(x,df);
		gsl_vector_sub(df,fx); // f(x+dx)-f(x)
        		for(int i=0;i<n;i++){
			gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);
			}
		gsl_vector_set(x,j,gsl_vector_get(x,j)-dx);
		}
  // Now I have an equation on the form J Dx=-f(x) which is solved for Dx
		qr_gs_decomp(J,R);
		gsl_vector_scale(fx,-1.0);
		qr_gs_solve(J,R,fx,Dx);
		double lambda=1.0;
		do{
			lambda/=2.0;
			gsl_vector_memcpy(xx,x);
            		gsl_blas_daxpy (lambda,Dx,xx);
                        f(xx,fxx);

		}while(gsl_blas_dnrm2(fxx)>(1.0-lambda/2.0)*gsl_blas_dnrm2(fx) && lambda>0.02);


     gsl_vector_memcpy(x,xx);
     gsl_vector_memcpy(fx,fxx);


  }while(gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(fx)>eps);


  gsl_vector_free(fx);
  gsl_vector_free(df);
  gsl_vector_free(Dx);
  gsl_vector_free(xx);
  gsl_vector_free(fxx);
  gsl_matrix_free(R);
  gsl_matrix_free(J);

  return calls;
}

void gradient_fit(double f(gsl_vector * x,gsl_vector * xd, gsl_vector * yd, gsl_vector *e), gsl_vector * x,gsl_vector * xd, gsl_vector * yd,gsl_vector * e, gsl_vector * df,double dx){
  double fx = f(x,xd,yd,e);
  int n = (*x).size;
  for (int i = 0; i < n; i++){
    gsl_vector_set(x,i,gsl_vector_get(x,i)+dx);
    gsl_vector_set(df,i,(f(x,xd,yd,e)-fx)/dx);
    gsl_vector_set(x,i,gsl_vector_get(x,i)-dx);
  }
}

int Quasi_Newton_fit(double f(gsl_vector * x,gsl_vector * xd,gsl_vector * yd, gsl_vector *e),gsl_vector * x, gsl_vector * xd, gsl_vector *yd, gsl_vector * e,double eps,double dx){
	int n=(*x).size;
	gsl_matrix * H=gsl_matrix_alloc(n,n);// H inverse
	gsl_matrix_set_identity (H);
	gsl_matrix * dH=gsl_matrix_alloc(n,n);// Correction term
	gsl_vector * dfx=gsl_vector_alloc(n);
	gsl_vector * dfz=gsl_vector_alloc(n);
	gsl_vector * s=gsl_vector_alloc(n);
	gsl_vector * z=gsl_vector_alloc(n);
	gsl_vector * y=gsl_vector_alloc(n);
	gsl_vector * Dx=gsl_vector_alloc(n);
	gsl_vector * H_times_y=gsl_vector_alloc(n);
	gsl_vector * sTH=gsl_vector_alloc(n);
	gsl_matrix * Numerator=gsl_matrix_alloc(n,n);
	gsl_vector * Hs=gsl_vector_alloc(n);
	gsl_vector * u=gsl_vector_alloc(n);

	gradient_fit(f,x,xd,yd,e,dfx,dx);
	double fx=f(x,xd,yd,e);

	int calls=0; double sdf, de, fz, alpha=0.1;
	do{ calls++;
		gsl_blas_dgemv (CblasNoTrans,1.0,H,dfx,0,Dx);
		gsl_vector_scale(Dx,-1);

		gsl_vector_memcpy(s,Dx);// setting s
		gsl_vector_scale(s,2);

	do{
		gsl_vector_scale(s,0.5);
		gsl_vector_memcpy(z,x);
		gsl_vector_add(z,s);
		fz=f(z,xd,yd,e);
		gsl_blas_ddot(s,dfx,&sdf);//Delta(x)*grad(f(x)) eq. 8
		if(fabs(fz) < fabs(fx) + alpha*sdf){break;}
		if(gsl_blas_dnrm2(s) < dx) {gsl_matrix_set_identity(H); break;}
	}while(fabs(fz) > fabs(fx) + alpha*sdf);

	gradient_fit(f,z,xd,yd,e,dfz,dx);
	gsl_vector_memcpy(y,dfz);
	gsl_vector_sub (y,dfx);// Grad(f(x))-grad(f(x+s))
	gsl_blas_dgemv(CblasNoTrans,1.0,H,y,0.0,H_times_y);

	// correction to H
	gsl_vector_memcpy(u,s);
	gsl_vector_sub(u,H_times_y);
	for (int i=0; i<n; i++) for (int j=0; j<n; j++)gsl_matrix_set(Numerator,i,j,gsl_vector_get(u,i)*gsl_vector_get(s,j));

	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,Numerator,H,0.0,dH);// Numerator eq 12
	gsl_blas_dgemv(CblasNoTrans,1.0,H,s,0.0,Hs);
	gsl_blas_ddot (y,Hs,&de); // Determinator

	gsl_matrix_scale (dH,1/de);
	gsl_matrix_add (H,dH);

	gsl_vector_memcpy(x,z);
	gsl_vector_memcpy(dfx,dfz);
	fx=fz;
  }while(gsl_blas_dnrm2(dfx)>eps);



  gsl_matrix_free(H);
  gsl_matrix_free(dH);
  gsl_vector_free(s);
  gsl_vector_free(dfx);
  gsl_vector_free(dfz);
  gsl_vector_free(y);
  gsl_vector_free(z);
  gsl_vector_free(Dx);
  gsl_vector_free(sTH);
  gsl_vector_free(H_times_y);
  gsl_matrix_free(Numerator);
return calls;
}
