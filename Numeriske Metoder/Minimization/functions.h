int newton(double f(gsl_vector* x),void gradient(gsl_vector * x, gsl_vector * df), void hessian(gsl_vector * x, gsl_matrix * H),
	gsl_vector * x,double eps);
void qr_gs_decomp(gsl_matrix* A, gsl_matrix *R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* x);
void outer_product(gsl_vector * x, gsl_vector * y, gsl_matrix * res);
int newtonRoot(void f(gsl_vector * x, gsl_vector * fx),gsl_vector * x,double dx,double eps);
int Quasi_Newton_fit(double f(gsl_vector * x,gsl_vector * xd,gsl_vector * yd, gsl_vector *e),gsl_vector * x, gsl_vector * xd, gsl_vector *yd, gsl_vector * e,double eps,double dx);
