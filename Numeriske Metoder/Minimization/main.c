#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"functions.h"
#include<time.h>


int print_vector(gsl_vector * M){// Function that prints vector
int n=(*M).size;
  for (int row=0; row<n; row++)

         printf("%8.3f     ", gsl_vector_get(M,row));

return 0;
}

int print_matrix(gsl_matrix * M){// Function that prints matrix
int n=(*M).size1, m=(*M).size2;
for (int row=0; row<n; row++)
{
    for(int columns=0; columns<m; columns++)
         printf("%8.3f     ", gsl_matrix_get(M,row,columns));
    printf("\n");
 }
return 0;
}

double Rosenbrock(gsl_vector * x)
{
  double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
  return pow(1-x1,2)+100*pow(x2-pow(x1,2),2);
}

void Rosenbrock_gradient(gsl_vector * x, gsl_vector * dfx)
{
   double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
   gsl_vector_set(dfx,0,-2*(1-x1)+200*(x2-x1*x1)*(-2*x1));
   gsl_vector_set(dfx,1,200*(x2-x1*x1));
}

void Rosenbrock_hessian(gsl_vector * x, gsl_matrix * J)
{
  double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
  gsl_matrix_set(J,0,0,1200*x1*x1-400*x2+2),  gsl_matrix_set(J,0,1,-400*x1);
  gsl_matrix_set(J,1,0,-400*x1),  gsl_matrix_set(J,1,1,200);
}



double Himmelblau(gsl_vector * x)
{
  double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
  return pow(pow(x1,2)+x2-11,2)+pow(x1+pow(x2,2)-7,2);
}

void Himmelblau_gradient(gsl_vector * x, gsl_vector * fx)
{
  double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
  gsl_vector_set(fx,0,2*(x1*x1+x2-11)*2*x1+2*(x1+x2*x2-7));
  gsl_vector_set(fx,1,2*(x1*x1+x2-11)+2*(x1+x2*x2-7)*2*x2);
}

void  Himmelblau_hessian(gsl_vector * x, gsl_matrix * J)
{
  double x1=gsl_vector_get(x,0), x2=gsl_vector_get(x,1);
  gsl_matrix_set(J,0,0,-42+12*x1*x1 + 4*x2),  gsl_matrix_set(J,0,1,4*(x1+x2));
  gsl_matrix_set(J,1,0,4*(x1+x2)),  gsl_matrix_set(J,1,1,4*x1+12*x2*x2-26);
}


int main(){
// Part A ----------------------------------------------------------------------
printf("PART A \n\n");

gsl_vector * x= gsl_vector_alloc(2);
double x1_i=4;
double x2_i=2;
double eps = 10e-7;
gsl_vector_set(x,0,x1_i);
gsl_vector_set(x,1,x2_i);
int step_rosenbrock = newton(Rosenbrock,Rosenbrock_gradient,Rosenbrock_hessian,x,eps);

printf("Minimum for Rosenbrock function (x,y)=");
print_vector(x);
printf("The minimum is found in %i steps \n ", step_rosenbrock );

printf("\n \nMinimum 1 for the Himmelblau function at (x,y)=");
x1_i=-2; x2_i=2;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
int step_Himmelblau=newton(Himmelblau,Himmelblau_gradient, Himmelblau_hessian,x,eps);
print_vector(x);
printf("done in %i steps",step_Himmelblau);

printf("\n \nMinimum 2 for the Himmelblau function at (x,y)=");
x1_i=2; x2_i=2;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
step_Himmelblau=newton(Himmelblau,Himmelblau_gradient, Himmelblau_hessian,x,eps);
print_vector(x);
printf("done in %i steps",step_Himmelblau);

printf("\n \nMinimum 3 for the Himmelblau function at (x,y)=");
x1_i=-4; x2_i=-4;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
step_Himmelblau=newton(Himmelblau,Himmelblau_gradient, Himmelblau_hessian,x,eps);
print_vector(x);
printf("done in %i steps",step_Himmelblau);

printf("\n \nMinimum 4 for the Himmelblau function at (x,y)=");
x1_i=4; x2_i=-3;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
step_Himmelblau=newton(Himmelblau,Himmelblau_gradient, Himmelblau_hessian,x,eps);
print_vector(x);
printf("done in %i steps\n\n",step_Himmelblau);

// PART B
printf("PART B \n\n");

printf("\n Minimum for the Rosenbrock function using Quasi-newton at (x,y)=");
x1_i=4, x2_i=2, eps=10e-7; double dx=10e-7;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
int step_Qu_N=Quasi_Newton(Rosenbrock,x,eps,dx);
print_vector(x);
printf("done in %i steps",step_Qu_N);

printf("\n \nMinimum 1 for the Himmelblau function using Quasi-newton at (x,y)=");
x1_i=-2, x2_i=2;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
step_Qu_N=Quasi_Newton(Himmelblau,x,eps,dx);
print_vector(x);
printf("done in %i steps",step_Qu_N);

printf("\n \nMinimum 2 for the Himmelblau function using Quasi-newton at (x,y)=");
x1_i=2, x2_i=2;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
step_Qu_N=Quasi_Newton(Himmelblau,x,eps,dx);
print_vector(x);
printf("done in %i steps",step_Qu_N);


printf("\n \nMinimum 3 for the Himmelblau function using Quasi-newton at (x,y)=");
x1_i=-4, x2_i=-4;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
step_Qu_N=Quasi_Newton(Himmelblau,x,eps,dx);
print_vector(x);
printf("done in %i steps",step_Qu_N);

printf("\n \nMinimum 4 for the Himmelblau function using Quasi-newton at (x,y)=");
x1_i=4, x2_i=-3;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
step_Qu_N=Quasi_Newton(Himmelblau,x,eps,dx);
print_vector(x);
printf("done in %i steps\n",step_Qu_N);

//Netwon's method from root finding exercise
printf("Netwon's method from root finding exercise\n");
printf("\n \nMinimum for the Rosenbrock function using Newton's methods from the root-finding exercise at (x,y)=");
x1_i=3, x2_i=2;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
int steps=newtonRoot(Rosenbrock_gradient,x,eps,dx);
print_vector(x);
printf("done in %i steps",steps);

printf("\n \nMinimum for the Himmelblau function using Newton's methods from the root-finding exercise at (x,y)=");
x1_i=-2, x2_i=2;
gsl_vector_set(x,0,x1_i); gsl_vector_set(x,1,x2_i);
steps=newtonRoot(Himmelblau_gradient,x,eps,dx);
print_vector(x);
printf("done in %i steps \n",steps);





  return 0;
}
