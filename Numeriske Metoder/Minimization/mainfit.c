#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"functions.h"


double master(gsl_vector * x ,gsl_vector * xd, gsl_vector * yd, gsl_vector *e){
	double f=0;
	double A=gsl_vector_get(x,0);
	double B=gsl_vector_get(x,1);
	double T=gsl_vector_get(x,2);
	for(int i=0; i<(*xd).size;i++){
		f+=pow(A*exp(-gsl_vector_get(xd,i)/T)+B-gsl_vector_get(yd,i),2)/pow(gsl_vector_get(e,i),2);

	}

	return f;
}


int main(){
// Apply your implementation to the following non-linear lieast-squares fitting problem:
double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int n = sizeof(t)/sizeof(t[0]);
	// Writing into gsl vectors
	gsl_vector * guess=gsl_vector_alloc(3); // Parameters
	gsl_vector * t1=gsl_vector_alloc(n);
	gsl_vector * y1=gsl_vector_alloc(n);
	gsl_vector * e1=gsl_vector_alloc(n);
	for(int i=0; i<n;i++){
		gsl_vector_set(t1,i,t[i]);
		gsl_vector_set(y1,i,y[i]);
		gsl_vector_set(e1,i,e[i]);
		printf("%g %g %g\n",t[i], y[i], e[i]);

	}

	gsl_vector_set(guess,0,3);
	gsl_vector_set(guess,1,1);
	gsl_vector_set(guess,2,1);
	double eps=10e-4;
	double step=10e-4;
	Quasi_Newton_fit(master,guess,t1,y1,e1,eps,step);
	double A=gsl_vector_get(guess,0), B=gsl_vector_get(guess,1), T=gsl_vector_get(guess,2);
	//printf("\n \n %g %g %g  \n",A,B,T);
	double f; printf("\n \n");
	for(double x=0; x<10;x+=0.01){
		f=0;
		f=A*exp(-x/T)+B;
		printf("%g %g\n",x,f);
	}

return 0;
}
