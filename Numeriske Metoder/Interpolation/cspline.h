typedef struct {int n; double *x, *y, *b, *c, *d;} cspline;
cspline * cspline_alloc(int n, double *x, double *y); /* builds the quadratic spline */
double cspline_evaluate(cspline *s, double z);        /* evaluates the spline at point z */
double cspline_derivative(cspline *s, double z); /* evaluates the derivative of the spline at point z */
double cspline_integral(cspline *s, double z);  /* evaluates the integral of the spline from x[0] to z */
void cspline_free(cspline *s); /* free memory allocated in qspline_alloc */
