typedef struct {int n; double *x, *y, *b, *c;} qspline;
qspline * qspline_alloc(int n, double *x, double *y); /* builds the quadratic spline */
double qspline_evaluate(qspline *s, double z);        /* evaluates the spline at point z */
double qspline_derivative(qspline *s, double z); /* evaluates the derivative of the spline at point z */
double qspline_integral(qspline *s, double z);  /* evaluates the integral of the spline from x[0] to z */
void qspline_free(qspline *s); /* free memory allocated in qspline_alloc */
