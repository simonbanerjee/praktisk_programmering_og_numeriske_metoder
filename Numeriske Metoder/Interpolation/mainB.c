#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include"qspline.h"


int main(){
// we set our inital , final and step size
double step_size = 0.5;
double initial = 0.0;
double final = 2*M_PI;

int steps = ceil((final-initial)/step_size);

// we allocate room for x and y
double *x=malloc(steps *sizeof(double));
double *y=malloc(steps *sizeof(double));

double n;
int s=0;

for (n = initial ; n <=final; n=n+step_size){
double function = cos(n);
fprintf(stderr, "%g %g\n",n,function);
x[s]=n , y[s]=function;
s++;
}

double z;
double initial_z=0.0;
double final_z = 2*M_PI;
double step_size_z=0.2;

// now we allocate space for z using our allocation function from qspline.c
qspline * qs_result= qspline_alloc(steps,x,y);

for (z = initial_z; z <= final_z; z=z+step_size_z){

double qinter= qspline_evaluate(qs_result,z);
double derivative = qspline_derivative(qs_result, z);
double integral = qspline_integral(qs_result,z);
printf("%g %g %g %g \n", z, qinter , derivative , integral);
}

qspline_free(qs_result);
  return 0;
}
