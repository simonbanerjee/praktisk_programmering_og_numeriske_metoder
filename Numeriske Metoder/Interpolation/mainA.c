#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<math.h>
#include<tgmath.h>

// From numeric book from the linear interpolation part
double linterp(int n , double *x, double *y, double z){
int i=0 , j=n-1;
while(j-i>1){
	int m=(i+j)/2; if (z>x[m]) i=m; else j=m;
	}
return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);
}

double linterp_integ(int n, double *x, double *y, double z){
  //assert(n>1 && z>=x[0] && z<=x[n-1]);
  int i=0, j=n-1; double integ;
  while(j-i>1){int m=(i+j)/2; if (z>x[m]) i=m; else j=m;}
  //return (y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);
  double a=(y[i+1]-y[i])/(x[i+1]-x[i]);
  double b=y[i];
  integ=(z-x[i])*b+a*(1.0/2.0*(pow(z,2)-pow(x[i],2))-x[i]*(z-x[i]));
  if(0<i)
	{
  		for(int q=0;q<i;q++)
		{
  		double a2=(y[q+1]-y[q])/(x[q+1]-x[q]);
  		double b2=y[q];
  		integ+=(x[q+1]-x[q])*b2+a2*(1.0/2.0*(pow(x[q+1],2)-pow(x[q],2))-x[q]*(x[q+1]-x[q]));
		}
  	}
  else {integ+=0;}

  return integ;
}

int main() {

// We create some test data
double step_size= 0.4;
double initial =0.0;
double final=2*M_PI;
int steps = ceil(final-initial)/step_size;

// We allocate memory for x and y
double *x = malloc(steps*sizeof(double));
double *y = malloc(steps*sizeof(double));

double n;
int s=0;

// We make some test data, here cosine is used.
for (n=initial; n<= final; n=n+step_size){
  double function = cos(n);
  fprintf(stderr, "%g %g\n", n, function);
  x[s]=n , y[s]=function;
s++;
}

// Now we make our for-loop that's gonna interpolate and integrate
double step_size_z=0.1;
double z;
double res;
for (z=initial;z<=final; z=z+step_size_z){

	res=linterp(steps,x,y,z);

	double intg=linterp_integ(steps, x, y, z);

	printf("%g %g %g\n",z,res,intg);
}

free(x);
free(y);
  return 0;
}
