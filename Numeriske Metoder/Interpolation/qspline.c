#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include"qspline.h"
#include<assert.h>

qspline *qspline_alloc( int n , double * x , double * y){
  qspline* s=(qspline *) malloc(sizeof (qspline)) ;
  (*s).b=(double*)malloc((n-1)*sizeof(double));
  (*s).c=(double*)malloc((n-1)*sizeof(double));// ci
  (*s).x=(double*)malloc(n*sizeof(double)); // copy of xi
  (*s).y=(double*)malloc(n*sizeof(double)); // copy of yi
  (*s).n=n; for(int i=0;i<n;i++){(*s).x[i]=x[i]; (*s).y[i]=y[i];}

  int i; double p[n-1], h[n-1]; //VLA from C99

for(i=0;i<n-1;i++){h[i]=x[i+1]-x[i]; p[i]=(y[i+1]-y[i])/h[i];}
  (*s).c[0]=0; // recursion up:

for(i=0;i<n-2;i++){(*s).c[i+1]=(p[i+1]-p[i]-(*s).c[i]*h[i])/h[i+1];}
  (*s).c[n-2]/=2; //recursion down :

for(i=n-3;i>=0;i--){(*s).c[i]=(p[i+1]-p[i]-(*s).c[i+1]*h[i+1])/h[i];}

for(i=0;i<n-1;i++){(*s).b[i]=p[i]-(*s).c[i]*h[i];}

  return s;
}


double qspline_evaluate(qspline *s, double z){
  double *x=(*s).x, *y=(*s).y, *b=(*s).b, *c=(*s).c;
  int n=(*s).n;
  int i=0, j=n-1;
  //assert(z>=x[0] && z<=x[n-1]);
  while(j-i>1){int m=(i+j)/2; if (z>x[m]) i=m; else j=m;} // bisection
  return y[i]+b[i]*(z-x[i])+c[i]*pow(z-x[i],2); // Interpolating polynomial
};


double qspline_derivative(qspline *s ,double z){
  double *x=(*s).x, *b=(*s).b, *c=(*s).c;
  int  n=(*s).n;
  int i=0, j=n-1;
  while(j-i>1){int m=(i+j)/2; if (z>x[m]) i=m; else j=m;}
  return 2*c[i]*(z-x[i])+b[i];
};

double qspline_integral(qspline *s, double z){
  double *x=(*s).x, *y=(*s).y, *b=(*s).b, *c=(*s).c;
  int n=(*s).n;
  int i=0, j=n-1;
  while(j-i>1){int m=(i+j)/2; if (z>x[m]) i=m; else j=m;}
  double integral=-1.0/6.0*(x[i]-z)*(2*(c[i]*pow(x[i]-z,2)+3*y[i])-3*b[i]*(x[i]-z));

  if (0<i){
  for(int q=0;q<i;q++){integral+=-1.0/6.0*(x[q]-x[q+1])*(2*(c[q]*pow(x[q]-x[q+1],2)+3*y[q])-3*b[q]*(x[q]-x[q+1]));}}
  else {integral+=0;}
  return integral;

}


void qspline_free (qspline *s){free((*s).x);free((*s).y);free((*s).b);free((*s).c);free(s);}
