#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "cspline.h"

int main() {

//first I make some test data using sine
const int n=10;
//double *x=malloc(n*sizeof(double));
//double *y=malloc(n*sizeof(double));
double x[n],y[n];

for(int i=0;i<n;i++)
{
	x[i]=i;
	y[i]=sin(i);
	//printf("%g %g\n",x[i],y[i]);
	fprintf(stderr, "%g %g \n",x[i],y[i] );
}
printf("\n\n");

cspline * cs_result=cspline_alloc(n, x, y);

double z;

for(z=x[0]; z<=x[n-1]; z=z+0.1){
	double cinter=cspline_evaluate(cs_result,z);
	double derivative=cspline_derivative(cs_result,z);
	double integral=cspline_integral(cs_result,z);
	printf("%g %g %g %g\n",z,cinter,derivative,integral);
}


cspline_free(cs_result);
//free(x);
//free(y);
return 0;
}
