#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cspline.h"
#include <assert.h>

cspline * cspline_alloc(int n, double *x, double *y)
{
cspline *s = (cspline*) malloc(sizeof(cspline));

(*s).x=(double*) malloc(n*sizeof(double));
(*s).y=(double*) malloc(n*sizeof(double));
(*s).b=(double*) malloc(n*sizeof(double));
(*s).c=(double*) malloc(n*sizeof(double));
(*s).d=(double*) malloc(n*sizeof(double));
(*s).n=n;
for (int i = 0; i < n; i++)
{
          (*s).x[i] = x[i];
          (*s).y[i] = y[i];
}

// we define our VLA
double h[n-1];
double p[n-1];

for (int j = 0; j < n-1; j++)
{
          h[j]=x[j+1] -x[j];
}

for (int j = 0; j < n-1; j++)
{
          p[j]=(y[j+1] - y[j])/h[j];

}

//building the tridiagonal system
double D[n];
double Q[n-1];
double B[n];

D[0]=2;
D[n-1]=2;
Q[0]=1;
B[0]=3*p[0];
B[n-1]=3*p[n-2]; // Gauss elimination

for(int i=0;i<n-2;i++)
{
  Q[i+1]=h[i]/h[i+1];
}

for(int i=0;i<n-2;i++)
{
  D[i+1]=2*h[i]/h[i+1]+2;
}

for(int i=0;i<n-2;i++)
{
  B[i+1]=3*(p[i]+p[i+1]*h[i]/h[i+1]);
}

for (int i = 0; i < n; i++)
{
  D[i]-=Q[i-1]/ D[i-1];
  B[i]-=B[i-1]/ D[i-1];
}
(*s).b[n-1]=B[n-1]/ D[n-1]; // Back substitution

for(int i=n-2;i>=0;i--)
{
  (*s).b[i]=(B[i]-Q[i]*(*s).b[i+1])/D[i];
}

for(int i=0;i<n-1;i++)
{
  (*s).c[i]=(-2*(*s).b[i]-(*s).b[i+1]+3*p[i])/h[i];
  (*s).d[i]=((*s).b[i]+(*s).b[i+1]-2*p[i])/pow(h[i],2);
}
return s;
}

  double cspline_evaluate(cspline * s, double z)
  {
  	assert(z >= s->x[0]);
  	assert(z <= s->x[s->n - 1]);
  	int i = 0, j = s->n - 1;	// binary search for the interval for z :
  	while (j - i > 1) {
  		int m = (i + j) / 2;
  		if (z > s->x[m])
  			i = m;
  		else
  			j = m;
  	}
  	double h = z - s->x[i];	// calculate the inerpolating spline :
  	return s->y[i] + h * (s->b[i] + h * (s->c[i] + h * s->d[i]));
  }


double cspline_derivative(cspline *s ,double z){
  double *x=(*s).x, *b=(*s).b, *c=(*s).c,  *d=(*s).d;
  int  n=(*s).n;
  int i=0, j=n-1;
  while(j-i>1){int m=(i+j)/2; if (z>x[m]) i=m; else j=m;}
  return 3*d[i]*pow(z-x[i],2)+2*c[i]*(z-x[i])+b[i];
};


double cspline_integral(cspline *s, double z){
  double *x=(*s).x, *y=(*s).y, *b=(*s).b, *c=(*s).c, *d=(*s).d;
  int n=(*s).n;
  int i=0, j=n-1;
  while(j-i>1)
  {
    int m=(i+j)/2;
    if (z>x[m]) i=m;
    else j=m;
  }

  double integral=1.0/12.0*(x[i]-z)*(6*b[i]*(x[i]-z)-4*c[i]*pow(x[i]-z,2)+3*(d[1]*pow(x[i]-z,3)-4*y[i]));

  if (0<i){
  for(int q=0;q<i;q++)
  {
    integral+=1.0/12.0*(x[q]-x[q+1])*(6*b[q]*(x[q]-x[q+1])-4*c[q]*pow(x[q]-x[q+1],2)+3*(d[q]*pow(x[q]-x[q+1],3)-4*y[q]));
  }
}
  else {integral+=0;}
  return integral;

}


void cspline_free(cspline *s)
{
free((*s).x);
free((*s).y);
free((*s).b);
free((*s).c);
free((*s).d);
free(s);
}
