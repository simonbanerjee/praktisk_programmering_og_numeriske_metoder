#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"neurons.h"
#define DIFF(x) pow(x,2)

// For 1D	

neuron* neuron_alloc(int n,double(*f)(double)){
	neuron* network = malloc(sizeof(neuron));
	network->n=n;
	network->f=f;
	network->data=gsl_vector_alloc(3*n);	/*	a,b,w for every n	*/
	return network;
}
void neuron_free(neuron* network){
	gsl_vector_free(network->data);
	free(network);
}

double neuron_feed_forward(neuron* network,double x){
	double output_sum=0;
	for(int i=0; i<network->n; i++){
	/*	3*i to skip to next neuron 	*/
		double a=gsl_vector_get(network->data,3*i+0);
		double b=gsl_vector_get(network->data,3*i+1);
		double w=gsl_vector_get(network->data,3*i+2); /* Weight */
		output_sum += network->f((x+a)/b) * w;
	}
	return output_sum;
}

void neuron_train(neuron* network,gsl_vector* x_train,gsl_vector* f_train){
	double delta(gsl_vector* p){
		gsl_vector_memcpy(network->data,p);
		double deviation_sum=0;
		for(int i=0; i<x_train->size; i++){
			double x=gsl_vector_get(x_train,i);
			double f=gsl_vector_get(f_train,i);
			double y=neuron_feed_forward(network,x);
			deviation_sum+=DIFF(y-f);	/* Fp(xk) - fk  (²?)*/
		}
		/*	Normalize output */
		return deviation_sum;
	}
	gsl_vector* p=gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p,network->data);
/*	Find paramseters minimizing the deviation */
	Quasi_Newton(delta,p,1e-3);
	gsl_vector_memcpy(network->data,p);
	gsl_vector_free(p);
}
