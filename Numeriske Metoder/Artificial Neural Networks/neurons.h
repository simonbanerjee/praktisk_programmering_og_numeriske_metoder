#ifndef HAVE_HEADER_H /* For multiple includes	*/

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

/*	Functions	*/
typedef struct {int n; double (*f)(double); gsl_vector* data;} neuron;
/*	1D	*/
neuron* neuron_alloc(int n, double(*f)(double));
void neuron_free(neuron* network);
double neuron_feed_forward(neuron* network, double x);
void neuron_train(neuron* network, gsl_vector* x, gsl_vector* y);

#define HAVE_HEADER_H
#endif
