#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<time.h>
#include"jacobi.h"
#include<gsl/gsl_eigen.h>
#define RND (double)rand()/RAND_MAX



int print_matrix(gsl_matrix * M){// Function that prints matrix
int n=(*M).size1, m=(*M).size2;
for (int row=0; row<n; row++)
{
    for(int columns=0; columns<m; columns++)
         printf("%10.3f     ", gsl_matrix_get(M,row,columns));
    printf("\n");
 }
return 0;
}

int print_vector(gsl_vector * M){// Function that prints vector
int n=(*M).size;
  for (int row=0; row<n; row++)

         printf("%10.3f     ", gsl_vector_get(M,row));

return 0;
}

int main(int argc, char** argv) {
srand((unsigned)time(NULL));

//we set the size of the matrix
int n=5;
if(argc>1)n=atoi(argv[1]);

// I allocate memory for the the A and B matrix

gsl_matrix * A=gsl_matrix_calloc (n,n);
gsl_vector * e=gsl_vector_calloc (n);
gsl_matrix * V=gsl_matrix_calloc (n,n);
gsl_matrix * Q=gsl_matrix_calloc (n,n);
gsl_matrix * D1=gsl_matrix_calloc (n,n);
gsl_matrix * D=gsl_matrix_calloc (n,n);



for(int i=0;i<n;i++)
{
  for(int j=i;j<n;j++ )
  {
    double x=RND;
    gsl_matrix_set(A,i,j,x);
    gsl_matrix_set(A,j,i,x);
 }
}


gsl_matrix_memcpy(Q,A);
int cycles = jacobi_cyc(Q,e,V);
if(n>9) {printf("n=%i\n",n); return 0;}

printf("Random matrix \n A =\n");
print_matrix(A);

printf("\n For n=%i, number of cycles = %i\n",n, cycles);

printf("\n Diagonalization of matrix \n A= \n");
print_matrix(Q);

printf("\n Eigenvalues for the matrix \n");
print_vector(e);

printf("\n \n Matrix containing eigenvectors\n");
print_matrix(V);

gsl_blas_dgemm (CblasTrans,CblasNoTrans, 1.0,V,A,0.0,D1);
gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,D1,V,0.0,D);
printf("\n \nDiagonalized matrix D=\n");
print_matrix(D);

gsl_matrix_free(A);
gsl_vector_free(e);
gsl_matrix_free(V);
gsl_matrix_free(Q);
gsl_matrix_free(D1);
gsl_matrix_free(D);
  return 0;
}
