#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<time.h>
#include"jacobi.h"
#include<gsl/gsl_eigen.h>
#define M_PI 3.14159265358979323846

int print_matrix(gsl_matrix * M){// Function that prints matrix
int n=(*M).size1, m=(*M).size2;
for (int row=0; row<n; row++)
{
    for(int columns=0; columns<m; columns++)
         printf("%10.3f     ", gsl_matrix_get(M,row,columns));
    printf("\n");
 }
return 0;
}

int print_vector(gsl_vector * M){// Function that prints vector
int n=(*M).size;
  for (int row=0; row<n; row++)

         printf("%10.3f     ", gsl_vector_get(M,row));

return 0;
}

int main() {
srand((unsigned)time(NULL));
int n=7;

// I allocate memory for my matrices and vectors

gsl_matrix *A = gsl_matrix_calloc(n,n);
gsl_matrix *V = gsl_matrix_calloc(n,n);
gsl_vector *e = gsl_vector_calloc(n);

gsl_matrix *VV = gsl_matrix_calloc(n,n);
gsl_vector *ee = gsl_vector_calloc(n);

gsl_matrix *A1 = gsl_matrix_calloc(n,n);
gsl_matrix *A2 = gsl_matrix_calloc(n,n);
gsl_matrix *A3 = gsl_matrix_calloc(n,n);
gsl_matrix *A4 = gsl_matrix_calloc(n,n);
gsl_matrix *A5 = gsl_matrix_calloc(n,n);

for(int i=0;i<n;i++)
{
  for(int j=0;j<n;j++)
 {
    double x=rand()/1000000.0;
    gsl_matrix_set(A,i,i,x);// Set dioagonal
 }
}
for(int i=0; i<n;i++)
{
  for(int j=i+1;j<n;j++ )
  {
    double x=rand()/1000000.0;
    gsl_matrix_set(A,i,j,x);
    gsl_matrix_set(A,j,i,x);
 }
}

printf("Random generated matrix\n A=\n");
print_matrix(A);

// i make some copies of A
gsl_matrix_memcpy(A1,A);
gsl_matrix_memcpy(A2,A);
gsl_matrix_memcpy(A3,A);
gsl_matrix_memcpy(A4,A);
gsl_matrix_memcpy(A5,A);


int N=5;

printf("The lowest eigenvalue is when the rotation angel is zero\n");
double lowest_eigenvalue = 0.0;
// A rotation angle of 0 gives the lowest eigenvalue, where a rotation angle of pi/2 gives the highest
int cycles_e_by_e_low= jacobi_e_by_e(A,e,V,N,lowest_eigenvalue);
printf("number of sweeps = %i\n \n", cycles_e_by_e_low);
printf("Now we make row by row elimination for the first %i rows \n",N);
print_matrix(A);
printf("\n \n Eigenvalues\n");
print_vector(e);
printf("\n \n Eigenvectors\n");
print_matrix(V);

//---------------------------------------------------------------------------------

// Now I find the largest eigenvalue, this is done simply by copying the above command
// and changing the rotation angle from 0 to pi/2

printf("\n \n The highest eigenvalue is when the rotaion angle is pi/2\n");
double highest_eigenvalue = M_PI/2.0;
int cycles_e_by_e_high= jacobi_e_by_e(A1,ee,VV,N,highest_eigenvalue);
printf("number of sweeps = %i\n \n", cycles_e_by_e_high);
printf("Now we make row by row elimination for the first %i rows \n",N);
print_matrix(A1);
printf("\n \n Eigenvalues\n");
print_vector(ee);
printf("\n \n Eigenvectors\n");
print_matrix(VV);

//---------------------------------------------------------------------------------
printf("\n \nComparing cyclic method with the row by row method\n");
int sweeps_cyc = jacobi_cyc(A2,e,V);
printf("\n The number of rotations it takes to diagonalize a matrix using the cyclic method is %i \n",sweeps_cyc);

int sweeps_row = jacobi_e_by_e(A3,e,V,n,0.0);
printf("\n The number of rotations it takes to diagonalize a matrix using the row by row method is %i \n",sweeps_row);


// Comparison between number of rotations ot takes to diagonalize a matrix using cyclic method vs number rotations it takes to calculate the lowest eigenvalue
  printf("\n \n Comparison between cyclic and row by row method:");
  int sweeps_cyc2=jacobi_cyc(A4,e,V);
  printf("\n  The number of rotations it takes to diagonalize a matrix by cyclic sweep %i",sweeps_cyc2);
  int sweeps_row2=jacobi_e_by_e(A5,e,V,1,0.0);
  printf("\n   and the number of rotations it takes to find the lowest eigenvalue in the row by row sweep %i \n",sweeps_row2);






  gsl_matrix_free(A);
  gsl_vector_free(e);
  gsl_matrix_free(V);
  gsl_matrix_free(A1);
  gsl_vector_free(ee);
  gsl_matrix_free(VV);
  gsl_matrix_free(A2);
  gsl_matrix_free(A3);
  gsl_matrix_free(A4);
  gsl_matrix_free(A5);


  return 0;
}
