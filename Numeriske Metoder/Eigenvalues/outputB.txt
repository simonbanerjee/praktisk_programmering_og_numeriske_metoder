Random generated matrix
 A=
  1955.383        404.222        252.371        919.221       1225.113       1976.664        461.718     
   404.222        686.928        742.683       2056.906        680.167       1841.676         15.376     
   252.371        742.683       1056.600       1613.198        729.089       1877.394         29.967     
   919.221       2056.906       1613.198       1098.993       1987.105       1421.421       2009.958     
  1225.113        680.167        729.089       1987.105       1691.511       1269.949        773.348     
  1976.664       1841.676       1877.394       1421.421       1269.949        141.781       1406.484     
   461.718         15.376         29.967       2009.958        773.348       1406.484       2010.105     
The lowest eigenvalue is when the rotation angel is zero
number of sweeps = 183
 
Now we make row by row elimination for the first 5 rows 
  1955.383          0.000         -0.000         -0.000          0.000          0.000         -0.000     
   404.222        686.928          0.000          0.000          0.000         -0.000          0.000     
   252.371        742.683       1056.600         -0.000          0.000          0.000         -0.000     
   919.221       2056.906       1613.198       1098.993          0.000         -0.000         -0.000     
  1225.113        680.167        729.089       1987.105       1691.511          0.000          0.000     
  1976.664       1841.676       1877.394       1421.421       1269.949        141.781       2515.431     
   461.718         15.376         29.967       2009.958        773.348       1406.484       2010.105     

 
 Eigenvalues
 -2875.999      -1286.026        188.696        666.412       1722.296          0.000          0.000     
 
 Eigenvectors
    -0.217         -0.288         -0.025          0.244          0.803          0.000          0.000     
    -0.433          0.191         -0.696          0.108         -0.316          0.000          0.000     
    -0.341         -0.092          0.679          0.200         -0.394          0.000          0.000     
     0.403         -0.682         -0.162         -0.126         -0.271          0.000          0.000     
    -0.119          0.276          0.134         -0.842          0.157          0.000          0.000     
     0.619          0.555          0.050          0.315          0.049          0.000          0.000     
    -0.301          0.155          0.084          0.255         -0.002          0.000          0.000     

 
 The highest eigenvalue is when the rotaion angle is pi/2
number of sweeps = 100
 
Now we make row by row elimination for the first 5 rows 
  1955.383         -0.000          0.000          0.000          0.000         -0.000          0.000     
   404.222        686.928         -0.000          0.000          0.000          0.000         -0.000     
   252.371        742.683       1056.600         -0.000          0.000          0.000         -0.000     
   919.221       2056.906       1613.198       1098.993          0.000          0.000          0.000     
  1225.113        680.167        729.089       1987.105       1691.511         -0.000          0.000     
  1976.664       1841.676       1877.394       1421.421       1269.949        141.781          0.370     
   461.718         15.376         29.967       2009.958        773.348       1406.484       2010.105     

 
 Eigenvalues
  8295.821       1930.100       1722.296        666.412        188.696          0.000          0.000     
 
 Eigenvectors
     0.338         -0.227         -0.803          0.244         -0.025          0.000          0.000     
     0.320         -0.279          0.316          0.108         -0.696          0.000          0.000     
     0.306         -0.354          0.394          0.200          0.679          0.000          0.000     
     0.490          0.133          0.271         -0.126         -0.162          0.000          0.000     
     0.398         -0.007         -0.157         -0.842          0.134          0.000          0.000     
     0.429         -0.149         -0.049          0.315          0.050          0.000          0.000     
     0.328          0.840          0.002          0.255          0.084          0.000          0.000     

 
Comparing cyclic method with the row by row method

 The number of rotations it takes to diagonalize a matrix using the cyclic method is 87 

 The number of rotations it takes to diagonalize a matrix using the row by row method is 186 

 
 Comparison between cyclic and row by row method:
  The number of rotations it takes to diagonalize a matrix by cyclic sweep 87
   and the number of rotations it takes to find the lowest eigenvalue in the row by row sweep 21 
