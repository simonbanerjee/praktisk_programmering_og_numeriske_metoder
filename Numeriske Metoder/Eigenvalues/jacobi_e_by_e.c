#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"jacobi.h"
int jacobi_e_by_e(gsl_matrix * A, gsl_vector * e, gsl_matrix * V,int N, double theta)
{
  int n=(*A).size1, changed, sweeps=0;
  gsl_vector * ee=gsl_vector_calloc(n);
  gsl_matrix * VV=gsl_matrix_calloc(n,n);
  gsl_matrix_set_identity(VV);
  int p, q;

 for(int i=0;i<n;i++){gsl_vector_set(ee,i,gsl_matrix_get(A,i,i));}// As the loop runs the diagonal in A becomes the eigenvalues which are inserted into e (diagonal A)

 for(p=0; p<N; p++)
 {
   do{changed=0; sweeps++;
   for(q=p+1;q<n;q++) // Upper part
   {
   // I now find the angle that zeros element
   double A_pp=gsl_vector_get(ee,p);
   double A_qq=gsl_vector_get(ee,q);
   double A_pq=gsl_matrix_get(A,p,q);
   double phi=1.0/2.0*atan2(2*A_pq,A_qq-A_pp)+theta;
   double s=sin(phi), c=cos(phi);
   double A_pp1=pow(c,2)*A_pp-2*s*c*A_pq+pow(s,2)*A_qq;
   double A_qq1=pow(s,2)*A_pp+2*s*c*A_pq+pow(c,2)*A_qq;

     if(A_pp1!=A_pp){
      changed=1; // Checking wheter the diagonal elemnts has changed
      gsl_vector_set(ee,p,A_pp1);
      gsl_vector_set(ee,q,A_qq1);
      gsl_matrix_set(A,p,q,0.0);

      for(int i=p+1;i<q;i++)
      {
       double A_pi=gsl_matrix_get(A,p,i), A_iq=gsl_matrix_get(A,i,q);
       gsl_matrix_set(A,p,i,c*A_pi-s*A_iq);
       gsl_matrix_set(A,i,q,s*A_pi+c*A_iq);
      }

      for(int i=q+1;i<n;i++)
      {
       double A_pi=gsl_matrix_get(A,p,i), A_qi=gsl_matrix_get(A,q,i);
       gsl_matrix_set(A,p,i,c*A_pi-s*A_qi);
       gsl_matrix_set(A,q,i,s*A_pi+c*A_qi);
      }
      for(int i=0;i<n;i++)
      {// Setting eigenvectors
       double A_ip=gsl_matrix_get(VV,i,p), A_iq=gsl_matrix_get(VV,i,q);
       gsl_matrix_set(VV,i,p,c*A_ip-s*A_iq);
       gsl_matrix_set(VV,i,q,s*A_ip+c*A_iq);
      }
     }
    else if (A_pp1==A_pp){gsl_vector_set(e,p,A_pp1);for(int i=0;i<n;i++)gsl_matrix_set(V,i,p,gsl_matrix_get(VV,i,p));}
    }
  }while(changed!=0);
  }
  gsl_vector_free(ee);
  gsl_matrix_free(VV);
  return sweeps;

}
